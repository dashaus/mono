var is_ajax_enabled = false;

// CARGAR Y CAMBIAR LA URL CON AJAX

function loadAndChange_URL_AJAX(go_to_url, name_url) {
    newRequestAJAX(go_to_url, 'GET', null, null);
    var actual_url = window.location.href;
    if (name_url != 'delete' && actual_url != go_to_url) {
        // si la redireccion viene de un link con el atributo "name" en
        // "delete" entonces no almacenamos en el historial la url ya que
        // una vez eliminado la reply dicha url es inservible y puede
        // provocar agujeros de seguridad

        // también, si la url viene de la misma url anterior, no hacer un
        // push en el historial ya que almacenaría la misma url varias veces
        // y no tiene sentido eso
        history.pushState(null, name_url, go_to_url);
    }
}

// CARGAR DICHEROS DINAMICAMENTE (JS y CSS)

function loadJSCSS (type, filepath, async) {
    // default param for 'async'
    var headElement = document.getElementsByTagName("head")[0];
    if (async == undefined)
        async = true;
    if (type == 'js') {
        var newScript = document.createElement("script");
        newScript.type = "text/javascript";
        var req = new XMLHttpRequest();
        req.open("GET", "/static/"+filepath, async); // 'false': synchronous.
        req.send();
        if (async == false) {
            if (req.status == 200) {
                newScript.text = req.responseText;
                headElement.appendChild(newScript);
            }
        } else if (async == true) {
            req.onloadend = function () {
                if (req.status == 200){
                    newScript.text = req.responseText;
                    headElement.appendChild(newScript);
                }
            }
        }
    } else if (type == 'css') {
        var newCSS = document.createElement('link');
        newCSS.setAttribute('rel', 'stylesheet');
        newCSS.setAttribute('href', "/static/"+filepath);
        headElement.appendChild(newCSS);
    } else {
        return false;
    }
}

// AJAX

function newRequestAJAX (url, method, send, type) {
    var xhr = new XMLHttpRequest();

    xhr.onloadstart = function (event) {
        loader.innerHTML = "Cargando ...";
    };
    xhr.onprogress = function (event) {
        loader.innerHTML = "Cargando ...";
    }
    xhr.onloadend = function (event) {
        if (this.status == 404) {
            loader.innerHTML = "Página no encontrada ...";
        }
        if (this.status == 500) {
            var obj = xhr.responseText;
            console.log(obj);
        }
        if (this.status == 200) {
            loader.innerHTML = "";
            var obj = xhr.responseText;
            //console.log(obj);
            var json = JSON.parse(obj);
            processing_JSON(json);

            // si ajax se activa por primera vez, entonces
            // habilitamos el evento popstate para poder acceder a los
            // botones atras y adelante en el historial de navegacion.
            if (first_request_ajax == false) {
                first_request_ajax = true;
                activate_popstate();
            }
        }
    };
    xhr.open(method, url, true);
    xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    xhr.setRequestHeader("Vary", "*");
    xhr.send(send);
}

// PROCESAR EL OBJETO JSON RECIBIDO POR AJAX

function processing_JSON(json) {
    if (json['Html']) {
        // Si existe un campo 'Html' entonces contiene
        // HTML a insertar en el documento
        current_section(json);

    } else if (json['redirect_to_url']) {
        // emulamos una redirección
        loadAndChange_URL_AJAX(json['redirect_to_url'], null);
        // si la redireccion proviene de las siguiente secciones dl condicional
        // tendrán que ver con el logueo/deslogueo dl usuario navegante
        if (json['referer'] == 'login_logout' || json['referer'] == 'register' || json['referer'] == 'banned') {
            check_user(json);
        }
    }

    // Actual sección cargada
    function current_section(json) {
        var content_loaded = json['Html'];
        var section_loaded = json['Section'];

        // Buscamos y ejecutamos el codigo de la sección
        // a cargar en el HTML
        //if (section_loaded == 'register'
        //    && files_loaded.indexOf(section_loaded) == -1) {
        console.log(section_loaded);
        if (section_loaded == 'register') {
            //files_loaded.push(section_loaded);
            loadJSCSS('js', 'scripts/register.js');
            loadJSCSS('css', 'styles/register.css');
        } else if (section_loaded == 'show_post') {
            //files_loaded.push(section_loaded);
            loadJSCSS('js', 'scripts/showpost.js');
            loadJSCSS('js', 'scripts/bbcode/bbcodeparser.js');
        } else if (section_loaded == 'vitra_cinema_gala_type_add_edit') {
            //files_loaded.push(section_loaded);
            loadJSCSS('js', 'scripts/utils/form2js.js');
            loadJSCSS('js', 'scripts/vitra/gala_type_edit.js');
            loadJSCSS('css', 'styles/vitra/cinema_add_edit.css');
            loadJSCSS('css', 'styles/vitra/two_column_add_edit.css');
        } else if (section_loaded == 'vitra_cinema_gala_add') {
            //files_loaded.push(section_loaded);
            loadJSCSS('js', 'scripts/utils/form2js.js');
            loadJSCSS('js', 'scripts/global/init_select_tags_to_country_cities_boroughs.js', false);
            loadJSCSS('js', 'scripts/vitra/gala_add.js');
            loadJSCSS('css', 'styles/vitra/cinema_add_edit.css');
            loadJSCSS('css', 'styles/vitra/gala_add_edit.css');
            loadJSCSS('css', 'styles/vitra/two_column_add_edit.css');
        } else if (section_loaded == 'vitra_cinema_gala_edit') {
            //files_loaded.push(section_loaded);
            loadJSCSS('js', 'scripts/utils/form2js.js');
            loadJSCSS('js', 'scripts/global/init_select_tags_to_country_cities_boroughs.js', false);
            loadJSCSS('js', 'scripts/vitra/gala_edit.js');
            loadJSCSS('css', 'styles/vitra/cinema_add_edit.css');
            loadJSCSS('css', 'styles/vitra/gala_add_edit.css');
            loadJSCSS('css', 'styles/vitra/two_column_add_edit.css');
        } else if (section_loaded == 'vitra_cinema_gala_ceremony_add_edit') {
            //files_loaded.push(section_loaded);
            loadJSCSS('js', 'scripts/utils/form2js.js');
            loadJSCSS('js', 'scripts/vitra/ceremony_add_edit.js');
            loadJSCSS('css', 'styles/vitra/ceremony_add_edit.css');
            loadJSCSS('css', 'styles/vitra/cinema_add_edit.css');
            loadJSCSS('css', 'styles/vitra/two_column_add_edit.css');
        } else if (section_loaded == 'vitra_cinema_professional_occupation_add_edit') {
            loadJSCSS('js', 'scripts/utils/form2js.js');
            loadJSCSS('js', 'scripts/vitra/prof_occupation_edit.js');
            loadJSCSS('css', 'styles/vitra/cinema_add_edit.css');
            loadJSCSS('css', 'styles/vitra/two_column_add_edit.css');
        } else if (section_loaded == 'vitra_cinema_professional_add_edit') {
            loadJSCSS('js', 'scripts/utils/form2js.js');
            loadJSCSS('js', 'scripts/global/init_select_tags_to_country_cities_boroughs.js', false);
            loadJSCSS('js', 'scripts/global/init_search_input_es.js', false);
            loadJSCSS('js', 'scripts/vitra/professional_add_edit.js');
            loadJSCSS('css', 'styles/vitra/cinema_add_edit.css');
            loadJSCSS('css', 'styles/vitra/two_column_add_edit.css');
        } else if (section_loaded == 'vitra_cinema_book_add_edit') {
            loadJSCSS('js', 'scripts/global/init_search_input_es.js');
            loadJSCSS('js', 'scripts/utils/form2js.js');
            loadJSCSS('js', 'scripts/vitra/book_edit.js');
            loadJSCSS('css', 'styles/vitra/cinema_add_edit.css');
            loadJSCSS('css', 'styles/vitra/two_column_add_edit.css');
        } else if (section_loaded == 'vitra_cinema_classification_add_edit') {
            loadJSCSS('js', 'scripts/utils/form2js.js');
            loadJSCSS('js', 'scripts/vitra/classification_edit.js');
            loadJSCSS('css', 'styles/vitra/cinema_add_edit.css');
            loadJSCSS('css', 'styles/vitra/two_column_add_edit.css');
        } else if (section_loaded == 'vitraCinemaFilmAddEdit') {
            loadJSCSS('js', 'scripts/js/global/init_search_input_es.js', false);
            loadJSCSS('js', 'scripts/js/utils/form2js.js');
            loadJSCSS('js', 'scripts/js/vitra/film_add.js');
            loadJSCSS('css', 'styles/vitra/film_add.css');
            loadJSCSS('css', 'styles/vitra/cinema_add_edit.css');
            loadJSCSS('css', 'styles/vitra/two_column_add_edit.css');
        } else if (section_loaded == 'vitra_cinema_film_episode_search') {
            loadJSCSS('js', 'scripts/global/init_search_input_es.js', false);
            loadJSCSS('js', 'scripts/utils/form2js.js');
            loadJSCSS('js', 'scripts/vitra/film_episode_search_edit.js');
            loadJSCSS('css', 'styles/vitra/cinema_add_edit.css');
        } else if (section_loaded == 'vitra_cinema_film_episode_add_edit') {
            loadJSCSS('js', 'scripts/utils/form2js.js');
            loadJSCSS('js', 'scripts/vitra/film_episode_search_edit.js');
            loadJSCSS('css', 'styles/vitra/cinema_add_edit.css');
        } else if (section_loaded == 'vitra_cinema_genre_add_edit') {
            loadJSCSS('js', 'scripts/utils/form2js.js');
            loadJSCSS('js', 'scripts/vitra/genre_add_edit.js');
            loadJSCSS('css', 'styles/vitra/cinema_add_edit.css');
            loadJSCSS('css', 'styles/vitra/genre_add_edit.css');
            loadJSCSS('css', 'styles/vitra/two_column_add_edit.css');
        } else if (section_loaded == 'vitra_general_country_add') {
            loadJSCSS('js', 'scripts/utils/form2js.js');
            loadJSCSS('css', 'styles/vitra/country_add_edit.css');
            loadJSCSS('css', 'styles/vitra/two_column_add_edit.css');
        } else if (section_loaded == 'vitra_general_country_edit') {
            loadJSCSS('js', 'scripts/utils/form2js.js');
            loadJSCSS('js', 'scripts/vitra/country_edit.js');
            loadJSCSS('css', 'styles/vitra/country_add_edit.css');
            loadJSCSS('css', 'styles/vitra/two_column_add_edit.css');
        } else if (section_loaded == 'vitra_general_country_city_add') {
            loadJSCSS('js', 'scripts/utils/form2js.js');
            loadJSCSS('css', 'styles/vitra/country_add_edit.css');
            loadJSCSS('css', 'styles/vitra/two_column_add_edit.css');
        } else if (section_loaded == 'vitra_general_country_city_edit_borough_add_edit') {
            loadJSCSS('js', 'scripts/utils/form2js.js');
            loadJSCSS('js', 'scripts/vitra/country_city_edit.js');
            loadJSCSS('css', 'styles/vitra/country_add_edit.css');
            loadJSCSS('css', 'styles/vitra/two_column_add_edit.css');
        } else if (section_loaded == 'vitra_cinema_specs_add') {
            loadJSCSS('js', 'scripts/utils/form2js.js');
            loadJSCSS('js', 'scripts/vitra/specs_add_edit.js');
            loadJSCSS('css', 'styles/vitra/two_column_add_edit.css');
            loadJSCSS('css', 'styles/vitra/cinema_add_edit.css');
        } else if (section_loaded == 'vitra_cinema_specs_edit') {
            loadJSCSS('js', 'scripts/utils/form2js.js');
            loadJSCSS('js', 'scripts/vitra/specs_add_edit.js');
            loadJSCSS('css', 'styles/vitra/two_column_add_edit.css');
            loadJSCSS('css', 'styles/vitra/cinema_add_edit.css');
        } else if (section_loaded == 'vitra_cinema_specs_styles_add_edit') {
            loadJSCSS('js', 'scripts/utils/form2js.js');
            loadJSCSS('js', 'scripts/vitra/specs_styles_add_edit.js');
            loadJSCSS('css', 'styles/vitra/two_column_add_edit.css');
            loadJSCSS('css', 'styles/vitra/cinema_add_edit.css');
        } else if (section_loaded == 'vitra_cinema_version_cut_add_edit') {
            loadJSCSS('js', 'scripts/utils/form2js.js');
            loadJSCSS('js', 'scripts/vitra/version_cut_edit.js');
            loadJSCSS('css', 'styles/vitra/two_column_add_edit.css');
            loadJSCSS('css', 'styles/vitra/cinema_add_edit.css');
        }

        if (section_loaded == 'vitra_cinema_galas_ceremony') {
            //var left_column = document.getElementById('left_column');
            //left_column.innerHTML = "";
            //left_column.innerHTML = content_loaded;
            contener.innerHTML = content_loaded;
        } else {
            // rellenamos el contenido recibido en el HTML
            contener.innerHTML = content_loaded;
        }

        // finalmente, convertirmos los enlaces
        // dl nuevo contenedor insertado.
        convert_HTMLlink_to_AJAXlink();
        convert_form_to_send_data_in_json();
    }

    function check_user(json) {
        var username = json['username'];
        var activation = json['activation'];
        if (username == null) {
            login_logout.innerHTML = '<a href="/account/login">Identificarse</a> | ' +
                '<a href="/account/register">Registrarse</a></span>';
        } else {
            login_logout.innerHTML = '<span>¡Hola, <span style="text-decoration: underline; font-weight: bold ">'
                +username+'</span>! <a href="/account/logout">Desconectarse</a></span>'
            if (activation == null) {
                login_logout.innerHTML = '| <a href="/account/logout">' +
                    'Error en su sesión. Por favor, vuelva a loguearse</a>';
            }
            if (activation == false) {
                login_logout.innerHTML += ' | <a href="/account/resend">Su cuenta no está activada</a>';
            }
            if (activation == 'banned') {
                login_logout.innerHTML += ' | <span>Su cuenta se encuentra baneada</span>';
            }
        }
    }
}

// // ACTIVAR NAVEGACION AJAX E HISTORY SI EL NAVEGADOR ES COMPATIBLE

function convert_HTMLlink_to_AJAXlink () {
    function change_behavior_click(link, type) {
        if (type == 'a') {
            link.addEventListener("click", function(event) {
                event.preventDefault();
                if (location.href != link.href) {
                    loadAndChange_URL_AJAX(link.href, link.name);
                }
            }, false);
        }
    }

    var enlaces = document.getElementsByTagName('a');
    for (var i=0; i<enlaces.length; i++) {
        var enlace = enlaces[i];
        change_behavior_click(enlace, 'a');
    }
}

// CAMBIAR EL COMPORTAMIENTO DE LOS FORMULARIOS

function change_behavior_form(link, is_ajax_enabled) {
    link.addEventListener("submit", function(event) {
        var url = link.action;
        var form = event.target;
        var inputs = form.getElementsByTagName('input');
        var input_to_send = [];
        var typeData = null;
        for (f = 0; f<inputs.length; f++) {
            if (inputs[f].type != 'submit') {
                var input = inputs[f];
                var input_name = input.name.split('.');
                input_to_send.push(inputs[f]);
                if (input_name.length > 1)
                    typeData = 'json';
            }
        }
        if (typeData != 'json') {
            console.log("no es requerido mandar el formdata en JSON");
            var formData = new FormData(form);
        } else {
            var data = form2js(form, '.',true);
            var dataToJson = JSON.stringify(data);
            var formData = new FormData();
            formData.append(typeData, dataToJson);
            console.log(dataToJson);
        }

        var is_form_delete = false;
        var is_input_correct = false;
        var classname = form.getAttribute('data-form-type');
        if (classname == 'form_delete') {
            is_form_delete = true;
            var enter = window.prompt('Escriba "DEL" para confirmar la acción');
            if (enter != null)
                enter = enter.toLowerCase();
            if (enter == 'del') {
                is_input_correct = true;
            }
        }

        // CHECKEAMOS QUE INTRODUCE "DEL" CORRECTAMENTE

        if (is_form_delete == true && is_input_correct == false) {
            event.preventDefault();
            return false;
        }

        // YA SEA QUE INTRODUCE CORRECTAMENTE "DEL" O SE TRATE DE OTR ACCIÓN
        // LO REDIRIGIMOS AL "ACTION" DEL FORM MEDIANTE AJAX O PETICION NORMAL

        if (is_ajax_enabled) {
            event.preventDefault();
            newRequestAJAX(url, form.method, formData, typeData);
        } else if (typeData == 'json') {
            event.preventDefault();
            var form_json = document.createElement('form');
            var input_hidden = document.createElement('input');
            form_json.setAttribute('action', form.getAttribute('action'));
            form_json.setAttribute('method', 'post');
            input_hidden.setAttribute('name', typeData);
            input_hidden.setAttribute('type', 'hidden');
            input_hidden.value = dataToJson;
            form_json.appendChild(input_hidden);
            form_json.submit();
        }
    }, false);
}

function convert_form_to_send_data_in_json () {
    var botonesEnlaces = document.getElementsByName('submit');
    for (var b=0; b<botonesEnlaces.length; b++) {
        var botonEnlace = botonesEnlaces[b];
        change_behavior_form(botonEnlace, is_ajax_enabled);
    }
}

// EVENTO POPSTATE DE WINDOW

function activate_popstate() {
    window.addEventListener("popstate", function(event) {
        newRequestAJAX(location.pathname, 'GET', null, null);
    }, false);
}

function layout_loaded () {
    first_request_ajax = false;
    loader = document.getElementById('loader');
    contener = document.getElementById('contener');
    login_logout = document.getElementsByName('login_logout')[0];

    if (new XMLHttpRequest() && Modernizr.history) {
        is_ajax_enabled = true;
        convert_HTMLlink_to_AJAXlink();
    }

    convert_form_to_send_data_in_json();
}

window.addEventListener('load', layout_loaded);