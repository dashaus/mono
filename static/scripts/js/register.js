// REGISTER (/account/register)

function register_loaded () {

    function data_exists (field, value) {
        //var data_url = encodeURI("http://"+window.location.hostname+':27080/monoculum/users/_find?criteria='+'{"'+field+'":"'+value+'"}');
        var data_url = encodeURI("http://"+window.location.hostname+':27080/monoculum/users/_find?criteria='+'{"'+field+'": {"$regex": "^'+value+'$", "$options": "i"}}');
        var xhr = new XMLHttpRequest();

        xhr.onloadend = function() {
            if (xhr.status == 200) {
                var jsonObj = JSON.parse(xhr.responseText);
                var exists = "";
                var span_exists = document.getElementById(field+"_exists");
                var elem = document.getElementsByName(field)[0];
                if (jsonObj.results[0]) {
                    exists = "Ya está registrado";
                    span_exists.innerHTML = exists;
                    elem.style.borderColor = "red";
                    return true;
                } else {
                    exists = "¡Disponible!";
                    span_exists.innerHTML = exists;
                    elem.style.borderColor = "green";
                    return false;
                }
            }
        }

        xhr.open("GET", data_url, true);
        xhr.send()
    }

    function validate_signup(name, value, ajax) {
        var elem = document.getElementsByName(name)[0];
        var span_exists = document.getElementById(name+"_exists");
        if (ajax == 'true') {
            if (span_exists.innerHTML != "") {
                span_exists.innerHTML = ""
            }
        }
        if(elem.checkValidity()) {
            elem.style.borderColor = "green";
            return true;
        } else {
            if (ajax == 'true' && span_exists.innerHTML != "") {
                span_exists.innerHTML = "";
            }
            elem.style.borderColor = "red";
            return false;
        }
    }

    function checkpasswords(name) {
        var pwd1 = document.getElementsByName('pwd1')[0];
        var pwd2 = document.getElementsByName('pwd2')[0];
        var pwdCurrentField = document.getElementsByName(name)[0];

        if (pwdCurrentField.name == 'pwd1') {
            if (validate_signup(name, null, 'false')) {
                if (pwd2.value != "" && pwdCurrentField.value != pwd2.value)
                    pwd2.style.borderColor = "red";
                if (pwd2.value != "" && pwdCurrentField.value == pwd2.value)
                    pwd2.style.borderColor = "green";
            }
        }

        if (pwdCurrentField.name == 'pwd2') {
            //if (pwdCurrentField.value == pwd1.value && pwd1.value != "")
            if (pwdCurrentField.value == pwd1.value && pwd1.validity.valid)
                pwdCurrentField.style.borderColor = "green";
            else
                pwdCurrentField.style.borderColor = "red";
        }
    }

    function validate_form() {
        var field_username = document.getElementsByName('username')[0];
        var field_email = document.getElementsByName('email')[0];
        var field_pwd1 = document.getElementsByName('pwd1')[0];
        var field_pwd2 = document.getElementsByName('pwd2')[0];

        if (field_username.value == "" || field_email.value == ""
            || field_pwd1.value == "" || field_pwd2.value == "") {
            return false;
        } else {
            return true;
        }

        if (field_username.style.borderColor == "red" || field_email.style.borderColor == "red"
            || field_pwd1.style.borderColor == "red" || field_pwd2.style.borderColor == "red") {
            return false;
        } else {
            return true;
        }
    }

    function event_for_register() {
        var username = document.getElementById('username');
        var email = document.getElementById('email');
        var pwd1 = document.getElementById('pwd1');
        var pwd2 = document.getElementById('pwd2');
        var submit = document.getElementById('submit');

        // campo username
        username.addEventListener('input', function(e) {
            validate_signup('username', true, 'true')
        }, false);
        username.addEventListener('change', function(e) {
            if (this.validity.valid) {
                data_exists(this.name, this.value);
            }
        }, false);

        // campo email
        email.addEventListener('input', function(e) {
            validate_signup('email', true, 'true')
        }, false);
        email.addEventListener('change', function(e) {
            if (this.validity.valid) {
                data_exists(this.name, this.value);
            }
        }, false);

        // campos passwords
        pwd1.addEventListener('input', function(e) {
            checkpasswords(this.name, this.value, 'false')
        }, false);
        pwd2.addEventListener('input', function(e) {
            checkpasswords(this.name, this.value, 'false')
        }, false);

        // boton submit
        submit.addEventListener('click', function(e) {
            return validate_form();
        })
    }

    event_for_register();
}



// VER SI EL WINDOW ESTA YA CARAGADO (ESTARÍA NAVEGANDO EN AJAX)
// O SI ES UNA CARGA NUEVA Y WINDOW DEBE CARGARSE

if (document.readyState == 'complete') {
    register_loaded();
} else {
    window.addEventListener('load', register_loaded);
}
