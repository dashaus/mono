function country_city_loaded () {

    var json_result = null;
    function data_exists (country, value_country, city, value_city, projection) {
        var data_url = encodeURI("http://"+window.location.hostname+':27080/monoculum/COUNTRIES/_find?criteria={"'+country+'":"'+value_country+'","'+city+'":"'+value_city+'"}&fields='+projection);
        var xhr = new XMLHttpRequest();

        xhr.onloadend = function() {
            if (xhr.status == 200) {
                var jsonObj = JSON.parse(xhr.responseText);
                if (jsonObj.results[0]) {
                    json_result = jsonObj;
                    return true;
                } else {
                    json_result = false;
                    return false;
                }
            }
        }

        xhr.open("GET", data_url, false);
        xhr.send()
    }

    var form_delete_borough = document.getElementById('form_delete_borough');
    var city_id = document.getElementById('_id');
    var new_borough = document.getElementById('new_borough');
    var boroughs = document.getElementsByName('borough');
    var borough_id = document.getElementById('borough_id');
    var borough_name_es = document.getElementById('borough_name_es');
    var borough_name_en = document.getElementById('borough_name_en');

    var borough_id_input = document.getElementById('borough_id_input');


    // PROPIEDADES INICIALES

    city_id.setAttribute('readonly', 'true');


    // EVENTS to LINKS

    new_borough.addEventListener('click', function(event){
        borough_id.removeAttribute('readonly');
        borough_id.value = '';
        borough_name_en.value = '';
        borough_name_es.value = '';
        form_delete_borough.style.display = 'none';
        borough_id_input.value = '';
    }, false);

    for (var x=0; x<boroughs.length; x++) {
        var current_borough = boroughs[x];
        current_borough.addEventListener('click', function(event){
            var _id_country = this.getAttribute('data-country');
            var id_city = this.getAttribute('id');
            data_exists('_id', _id_country, 'boroughs._id', id_city, '{"boroughs.$":1}');
            fill_data_borough(json_result);
        }, false);

        function fill_data_borough (json) {
            json = json.results[0]['boroughs'][0];
            borough_id.setAttribute('readonly', 'true');
            borough_id.value = json['_id'];
            if (json['name']['es']) {
                borough_name_es.value = json['name']['es'];
            }
            borough_name_en.value = json['name']['en'];
            form_delete_borough.style.display = 'inline';
            borough_id_input.value = json['_id'];
        }
    }


}

// Comprobar si "document" está ya cargado (estaría en navegacion ajax)
// o, por lo contrario, añadir un listener para "window"

if (document.readyState == 'complete') {
    country_city_loaded();
} else {
    window.addEventListener('load', country_city_loaded);
}