function professional_loaded () {

    /*
     * Inicializar tags de select para country, city y borough
     */

    init_select_tags_to_country_cities_boroughs(true);


    /*
     * Initialize the search input to edit a professional specified
     */

    var input_search = document.getElementById('search_professional');
    var div_suggester = document.getElementById('suggester');
    var input_item_to_edit = document.getElementById('input_item_to_edit');
    var contener_citizenships = document.getElementById('citizenships');
    var contener_occupations = document.getElementById('occupations');
    var select_countries = document.getElementById('countries');
    var select_cities = document.getElementById('cities');
    var select_boroughs = document.getElementById('boroughs');

    var input_realname = document.getElementById('realname');
    var input_artname = document.getElementById('artname');
    var input_dateborn = document.getElementById('dateborn');
    var input_datedied = document.getElementById('datedied');
    var textarea_bio = document.getElementById('bio');

    var title_pro_edit = document.getElementById('title_pro_edit');
    var contener_actions_edit = document.getElementById('contener_actions_edit');
    var button_cancel_edit = document.getElementById('button_cancel_edit');


    button_cancel_edit.addEventListener('click', function (event){
        reset_all_fields();
    }, false);

    function query_to_search (text) {
        var json_query = '"query":{"multi_match":{"query":"'+text+'","fields":["artname", "realname", "_id"],"fuzziness": 2,"prefix_length": 1,"analyzer":"simple"}}';
        return json_query;
    }
    function remove_textNodes () {
        input_search.value = '';
    }

    function click_suggest (suggest) {
        reset_all_fields();
        title_pro_edit.innerHTML = suggest.data['artname'];
        contener_actions_edit.style.display = 'inline';
        for (var i in suggest.data) {
            var current_key = i;
            var current_value = suggest.data[i];
            if (current_key == '_id') {
                var id_input_hidden = document.createElement('input');
                id_input_hidden.setAttribute('type', 'hidden');
                id_input_hidden.setAttribute('name', 'edit');
                id_input_hidden.value = current_value;
                input_item_to_edit.appendChild(id_input_hidden);
            } else if (current_key != 'slugname') {
                var get_element = document.getElementById(current_key);
                if (current_key == 'dateborn' || current_key == 'datedied')
                    current_value = current_value.slice(0, 10);
                if (get_element.tagName == "INPUT" || get_element.tagName == "TEXTAREA")
                    get_element.value = current_value;
                else if (get_element.tagName == "DIV") {
                    if (current_key == 'citizenships') {
                        var checkbox = get_element.getElementsByTagName('input');
                        for (var y in current_value) {
                            var citizenship_pro = current_value[y];
                            for (var x=0; x<checkbox.length; x++) {
                                var citizenship = checkbox[x];
                                if (citizenship.value == citizenship_pro) {
                                    citizenship.checked = true;
                                    break;
                                }
                            }
                        }
                    } else if (current_key == 'occupations') {
                        var checkbox = get_element.getElementsByTagName('input');
                        for (var l in current_value) {
                            var occupation_pro = current_value[l];
                            for (var n=0; n<checkbox.length; n++) {
                                var occupation = checkbox[n];
                                if (occupation.value == occupation_pro) {
                                    occupation.checked = true;
                                    break;
                                }
                            }
                        }
                    } else if (current_key == 'placeborn') {
                        if (current_value['country']) {
                            find_option('country', select_countries);
                            emitEvent(select_countries, 'change');
                            if (current_value['city']) {
                                find_option('city', select_cities);
                                emitEvent(select_cities, 'change');
                                if (current_value['borough']) {
                                    find_option('borough', select_boroughs);
                                    emitEvent(select_boroughs, 'change');
                                }
                            }
                        }

                    }
                }
                //console.log(current_key,current_value, typeof current_value);
            }
        }
        function emitEvent(element, type) {
            check_auto_citizenship = false;
            if ("createEvent" in document) {
                var event = document.createEvent("HTMLEvents");
                event.initEvent(type, false, true);
                element.dispatchEvent(event);
            }
            else
                element.fireEvent("on"+type);
            // reset value
            check_auto_citizenship = true;
        }
        function find_option (value, select) {
            var get_options = select.getElementsByTagName('option');
            select.style.display = 'inline';
            for (var a=0; a<get_options.length; a++) {
                // evitamos comparar la opcion por defecto con value "0"
                if (a > 0) {
                    var current_option = get_options[a];
                    if (current_value[value] == current_option.value) {
                        current_option.selected = true;
                        break;
                    }
                }
            }
        }
    }

    init_search_input_es('professionals', input_search, div_suggester, query_to_search, 'artname', remove_textNodes, '_source', click_suggest, false);

    // Functions UTILS

    function remove_content_of_element (element, all) {
        if (all == true) {
            while (element.lastChild) {
              element.removeChild(element.lastChild);
            }
        } else {
            while (element.lastChild && element.children.length > 1) {
              element.removeChild(element.lastChild);
            }
        }
    }

    function uncheckall (contener) {
        var checkboxs = contener.getElementsByTagName('input');
        for (var i=0; i<checkboxs.length;i++) {
            var current_checkbox = checkboxs[i];
            current_checkbox.checked = false;
        }
    }

    function reset_all_fields () {
        remove_content_of_element(input_item_to_edit, true);
        remove_content_of_element(select_cities, false);
        remove_content_of_element(select_boroughs, false);
        select_cities.style.display = 'none';
        select_boroughs.style.display = 'none';
        uncheckall(contener_citizenships);
        uncheckall(contener_occupations);
        input_realname.value = "";
        input_artname.value = "";
        input_dateborn.value = "";
        input_datedied.value = "";
        textarea_bio.value = "";
        title_pro_edit.innerHTML = '';
        contener_actions_edit.style.display = 'none';
        select_countries.options[0].selected = true;
    }
}



// Comprobar si "document" está ya cargado (estaría en navegacion ajax)
// o, por lo contrario, añadir un listener para "window"

if (document.readyState == 'complete')
    professional_loaded();
else
    window.addEventListener('load', professional_loaded);