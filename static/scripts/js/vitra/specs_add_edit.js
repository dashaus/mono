function specs_add_loaded () {
    var type_radio = document.getElementsByName('type');
    var checkbox_episodes = document.getElementById('episodes');
    var aditional_option = document.getElementById('aditional_option');

    checkbox_episodes.default_checked = checkbox_episodes.checked;

    for (var i=0; i<type_radio.length; i++) {
        var current_radio = type_radio[i];
        current_radio.onchange = function (event) {
            if (this.value == 'format') {
                aditional_option.style.display = 'block';
                checkbox_episodes.checked = checkbox_episodes.default_checked;
            } else {
                aditional_option.style.display = 'none';
                checkbox_episodes.checked = false;
            }
        }
    }
}

// Comprobar si "document" está ya cargado (estaría en navegacion ajax)
// o, por lo contrario, añadir un listener para "window"

if (document.readyState == 'complete') {
    specs_add_loaded();
} else {
    window.addEventListener('load', specs_add_loaded);
}