function specs_styles_loaded () {
    function get_tecnic_style (field, projection) {
        var data_url = encodeURI("http://"+window.location.hostname+':27080/monoculum/FILM_FORMAT_CATEGORY_PRODUCTION/_find?criteria={'+field+'}&fields={'+projection+'}');
        var xhr = new XMLHttpRequest();

        xhr.onloadend = function() {
            if (xhr.status == 200) {
                var json_obj = JSON.parse(xhr.responseText);
                if (json_obj.results[0]) {
                    fill_data(json_obj);
                    return json_obj;
                } else {
                    console.log("no existe");
                    return false;
                }
            }
        }

        xhr.open("GET", data_url, true);
        xhr.send();
    }

    var links_edit_tecnic = document.getElementsByName('edit_tecnic');
    var links_edit_style = document.getElementsByName('edit_style');
    var radio_tecnic = document.getElementById('radio_tecnics');
    var radio_style = document.getElementById('radio_styles');
    var name_es = document.getElementById('name_es');
    var name_en = document.getElementById('name_en');
    var description_es = document.getElementById('description_es');
    var description_en = document.getElementById('description_en');
    var div_edit_item = document.getElementById('edit_item_selected');
    var tecnic_or_style = null;

    var title_from_edit = document.getElementById('title_from_edit');
    var button_cancel_edit = document.getElementById('button_cancel_edit');
    var contener_actions_edit = document.getElementById('contener_actions_edit');
    var type_to_delete = document.getElementById('type_to_delete');
    var id_to_delete = document.getElementById('id_to_delete');

    /*
    Links para editar/eliminar tecnica o estilo seleccionado
    */

    for (var i=0; i<links_edit_tecnic.length; i++) {
        var current_tecnic = links_edit_tecnic[i];
        current_tecnic.addEventListener('click', function(event){
            var id_tecnic_selected = this.getAttribute('id');
            tecnic_or_style = 'tecnics';
            get_tecnic_style('"tecnics._id":"'+id_tecnic_selected+'"', '"tecnics.$":1, "_id":0');
        }, false);
    }
    for (var p=0; p<links_edit_style.length; p++) {
        var current_style = links_edit_style[p];
        current_style.addEventListener('click', function(event){
            var id_style_selected = this.getAttribute('id');
            tecnic_or_style = 'styles';
            get_tecnic_style('"styles._id":"'+id_style_selected+'"', '"styles.$":1, "_id":0');
        }, false);
    }

    function fill_data (json) {
        reset_form();
        json = json.results[0];
        for (var i in json) {
            var tecnic_style_selected = json[i][0];
            contener_actions_edit.style.display = 'inline';
            type_to_delete.value = i;
            id_to_delete.value = tecnic_style_selected._id;
            if (tecnic_style_selected.name.es) {
                name_es.value = tecnic_style_selected.name.es;
                title_from_edit.innerHTML = tecnic_style_selected.name.es;
            } else {
                title_from_edit.innerHTML = tecnic_style_selected.name.en;
            }
            name_en.value = tecnic_style_selected.name.en;
            description_en.innerHTML = tecnic_style_selected.description.en;
            description_es.innerHTML = tecnic_style_selected.description.es;
            var get_radio = document.getElementById('radio_'+tecnic_or_style);
            get_radio.checked = true;
            var input_id = document.createElement('input');
            input_id.setAttribute('type', 'hidden');
            input_id.setAttribute('name', 'edit._id');
            input_id.value = tecnic_style_selected._id;
            var input_type = document.createElement('input');
            input_type.setAttribute('type', 'hidden');
            input_type.setAttribute('name', 'edit.type');
            input_type.value = i;
            div_edit_item.appendChild(input_id);
            div_edit_item.appendChild(input_type);
        }
    }

    /*
    Botton to cancel selected edition
    */

    button_cancel_edit.addEventListener('click', function(event){
        reset_form();
    }, false);


    function reset_form () {
        remove_content_of_element(div_edit_item);
        title_from_edit.innerHTML = '';
        contener_actions_edit.style.display = 'none';
        name_en.value = '';
        name_es.value = '';
        description_en.innerHTML = '';
        description_es.innerHTML = '';
        radio_tecnic.checked = false;
        radio_style.checked = false;
    }

    // Functions UTILS

    function remove_content_of_element (element) {
        while (element.lastChild) {
          element.removeChild(element.lastChild);
        }
    }
}



// Comprobar si "document" está ya cargado (estaría en navegacion ajax)
// o, por lo contrario, añadir un listener para "window"

if (document.readyState == 'complete')
    specs_styles_loaded();
else
    window.addEventListener('load', specs_styles_loaded);