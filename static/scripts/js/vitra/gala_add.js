function gala_loaded () {
    init_select_tags_to_country_cities_boroughs(false);
}

// Comprobar si "document" está ya cargado (estaría en navegacion ajax)
// o, por lo contrario, añadir un listener para "window"

if (document.readyState == 'complete') {
    gala_loaded();
} else {
    window.addEventListener('load', gala_loaded);
}