function book_loaded () {
    function get_item (collection, field, value, get_writers, input_button) {
        var data_url = encodeURI("http://"+window.location.hostname+':27080/monoculum/'+collection+'/_find?criteria='+'{"'+field+'":"'+value+'"}');
        var xhr = new XMLHttpRequest();

        xhr.onloadend = function() {
            if (xhr.status == 200) {
                var jsonObj = JSON.parse(xhr.responseText);
                if (jsonObj.results) {
                    if (get_writers == false)
                        fill_form(jsonObj);
                    else {
                        fill_and_append_button_writer_in_div(jsonObj, input_button);
                    }
                    return true;
                } else {
                    console.log("no encontrado");
                    return false;
                }
            }
        };

        xhr.open("GET", data_url, true);
        xhr.send();
    }



    /*
    * Initial SEARCH AUTHOR div editable tag
    */

    var writer_div = document.getElementsByName('writers')[0];
    var div_suggester = document.getElementById('suggester');

    function query_to_search_author (text) {
        var json_query = '"fields":["artname","realname","_id"],"query":{"multi_match":{"query":"'+text+'","fields":["artname", "realname"],"fuzziness":2,"prefix_length":1,"analyzer":"simple" }}, "filter":{"query":{"match":{"occupations":"3qm3LsHP"}}}';
        return json_query;
    }

    init_search_input_es('professionals', writer_div, div_suggester, query_to_search_author, 'artname', false, 'fields', false, true);

    /*
    * Links para editar/eliminar los libros
    */

    var links_edit = document.getElementsByName('link_edit');

    var name_original = document.getElementById('name_original');
    var name_es = document.getElementById('name_es');
    var name_en = document.getElementById('name_en');

    var contener_actions_edit = document.getElementById('contener_actions_edit');
    var title_from_edit = document.getElementById('title_from_edit');
    var div_edit_item = document.getElementById('edit_item_selected');
    var id_to_delete = document.getElementById('id_to_delete');

    var name_div_writers = writer_div.getAttribute('name');
    var writers_inputs_hidden = document.getElementById(name_div_writers);

    for (var i=0; i<links_edit.length; i++){
        var current_link = links_edit[i];
        current_link.addEventListener('click', function(event){
            event.preventDefault();
            reset_form();
            get_item('books','_id', this.id, false, false);
        }, false);
    }

    function fill_form (json) {
        json = json.results[0];
        name_original.value = '';
        name_en.value = '';
        name_es.value = '';
        contener_actions_edit.style.display = 'inline';
        id_to_delete.value = json['_id'];
        name_original.value = json['name']['original'];
        if (json['name']['es']) {
            title_from_edit.innerHTML = json['name']['es'];
            name_es.value = json['name']['es'];
        } else if (json['name']['en']) {
            title_from_edit.innerHTML = json['name']['en'];
            name_en.value = json['name']['en'];
        }

        var name_div_writers = writer_div.getAttribute('name');
        var writers_inputs_hidden = document.getElementById(name_div_writers);

        for (var i=0; i<json.writers.length; i++) {
            var writer = json.writers[i];
            var input_button = create_input('button', 'button_professional', null, null);
            input_button.id = writer.id;
            input_button.value = writer.id;
            get_item('professionals', '_id', writer.id, true, input_button);
            var input_id = create_input('hidden', null, name_div_writers+'['+i+'].id', null);
            var input_pos = create_input('hidden', null, name_div_writers+'['+i+'].position', null);
            input_id.removeAttribute('class');
            input_pos.removeAttribute('class');
            input_id.value = writer.id;
            input_pos.value = writer.position;
            writers_inputs_hidden.appendChild(input_id);
            writers_inputs_hidden.appendChild(input_pos);
        }

        var input_edit_id = document.createElement('input');
        input_edit_id.setAttribute('type', 'hidden');
        input_edit_id.setAttribute('name', 'edit');
        input_edit_id.value = json['_id'];
        div_edit_item.appendChild(input_edit_id);
    }

    function fill_and_append_button_writer_in_div (json,input_button) {
        json = json.results[0];
        input_button.value = json['artname'];
        writer_div.appendChild(input_button);
    }

    function create_input (type, classname, name, width) {
        var input = document.createElement('input');
        input.setAttribute('type', type);
        input.setAttribute('class', classname);
        input.setAttribute('name', 'add.'+name);
        input.style.width = width;
        return input;
    }

    /*
    Botton to cancel selected edition
    */

    button_cancel_edit.addEventListener('click', function (event){
        reset_form();
    }, false);


    function reset_form () {
        remove_content_of_element(div_edit_item);
        remove_content_of_element(writer_div);
        remove_content_of_element(writers_inputs_hidden);
        title_from_edit.innerHTML = '';
        contener_actions_edit.style.display = 'none';
        name_original.value = '';
        name_en.value = '';
        name_es.value = '';
    }

    // Functions UTILS

    function remove_content_of_element (element) {
        while (element.lastChild) {
          element.removeChild(element.lastChild);
        }
    }
}

// Comprobar si "document" está ya cargado (estaría en navegacion ajax)
// o, por lo contrario, añadir un listener para "window"

if (document.readyState == 'complete') {
    book_loaded();
} else {
    window.addEventListener('load', book_loaded);
}