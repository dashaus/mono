function film_loaded () {
    function get_item (search, collection, function_to_execute, element) {
        if (search != false)
            var data_url = encodeURI("http://"+window.location.host+'/api/'+collection+'/find?search={'+search+'}');
        else
            var data_url = encodeURI("http://"+window.location.host+'/api/'+collection+'/find?search={}');
        var xhr = new XMLHttpRequest();
        xhr.onloadend = function() {
            if (xhr.status == 200) {
                var json = JSON.parse(xhr.responseText);
                if (json)
                    function_to_execute(json, collection, element);
                else
                    console.log("ha fallado al parsearlo");
            }
        }
        xhr.open("GET", data_url, true);
        xhr.send();
    }

    // VAR GLOBALS


    // 1.0 Get ELEMENTS to DOM
    var all_idioms_json = {};
    var all_countries_title_json = {};
    var all_countries_json = {};
    var all_cutversion_json = {};

    var new_title = document.getElementById('new_title');
    var new_release = document.getElementById('new_release');
    var new_cut_version = document.getElementById('new_cut_version');

    var title_item_edit = document.getElementById('title_item_edit');
    var contener_actions_edit = document.getElementById('contener_actions_edit');
    var button_cancel_edit = document.getElementById('button_cancel_edit');
    var input_item_to_edit = document.getElementById('input_item_to_edit');
    var id_item_to_delete = document.getElementById('id_item_to_delete');

    var titlelangs_default = document.getElementsByClassName('input_titlelang');
    var releasecountries_default = document.getElementsByClassName('input_releasecountry');
    var select_new_title = document.getElementById('select_new_title');
    var select_new_country = document.getElementById('select_new_country');

    var contener_titlelangs = document.getElementById('contener_titlelang');
    var contener_releasecountry = document.getElementById('contener_releasecountry');
    var select_new_releasecountry = document.getElementById('select_new_release');

    var cutversion_default = document.getElementsByClassName('option_cutversion');
    var contener_cutversion = document.getElementById('contener_cutversion');
    var select_new_cutversion = document.getElementById('select_new_cut_version');

    var inputs_crew = document.getElementsByName('crew');
    var inputs_genre_topic = document.getElementsByName('genres_topics');
    var input_cast = document.getElementById('cast');
    var div_suggester = document.getElementById('suggester');
    var inputs_hidden_crew = document.getElementById('inputs_hidden_crew');

    var DB_NAME_COUNTRY = 'COUNTRIES';
    var DB_NAME_VERSIONCUT = 'FILM_VERSION_CUT';


    /*
    * Event to FORMAT select tag
    */

    var select_format = document.getElementById('select_format');
    var cont_duration_cut = document.getElementById('cont_duration_cut');
    var cont_duration_episode = document.getElementById('cont_duration_episode');
    var input_cut = cont_duration_cut.getElementsByTagName('input')[0];
    var input_episode = cont_duration_episode.getElementsByTagName('input')[0];
    var input_cut_default = document.getElementById('input_cut_default');
    var select_cut_default = document.getElementById('select_cut_default');

    select_format.onchange = function (event) {
        var index_selected = this.selectedIndex;
        var option_selected = this.options[index_selected];
        if (option_selected.getAttribute('data-episodes')) {
            input_cut_default.setAttribute('disabled', '');
            select_cut_default.setAttribute('disabled', '');
            switch_duration(cont_duration_episode, cont_duration_cut, input_episode,input_cut, false);
    } else {
            switch_duration(cont_duration_cut, cont_duration_episode, input_cut, input_episode, true);
        }
    }

    function switch_duration (cont_visible, cont_hidden, input_enabled, input_disabled, remove_disabled) {
        cont_visible.style.display = 'block';
        input_enabled.setAttribute('required', '');
        input_enabled.removeAttribute('disabled');
        input_enabled.value = '';
        cont_hidden.style.display = 'none';
        input_disabled.removeAttribute('required');
        input_disabled.setAttribute('disabled','');
        input_disabled.value = '';

        if (remove_disabled == false) {
            input_cut_default.setAttribute('disabled', '');
            select_cut_default.setAttribute('disabled', '');
        } else {
            input_cut_default.removeAttribute('disabled');
            select_cut_default.removeAttribute('disabled');
        }
    }


    /*
    * Initial TITLE select tag, RELEASE select tag, VERSION/CUT select tag
    */

    get_item(false, DB_NAME_COUNTRY, initial_TitleAndRelease_SelectTags);
    get_item(false, DB_NAME_VERSIONCUT, initial_CutVersion_SelectTags);

    // Functions UTILS
    function remove_existent (elements_default, json, value, attribute, regex) {
        for (var i=0; i<elements_default.length; i++) {
            var current_input = elements_default[i];
            var name_input = current_input[attribute];
            if (regex)
                name_input = name_input.match(/[A-Za-z_]+$/)[0];
            for (var x in json) {
                var current_country = json[x];
                var value_current_country = current_country[value];
                if (name_input == value_current_country) {
                    delete json[x];
                }
            }
        }
    }
    function append_options_to_select (json, value, contener, i18n, link_add) {
        if (link_add != null)
            link_add.style.display = 'inline-block';
        contener.style.display = 'inline-block';
        for (var i in json) {
            var current_country = json[i];
            var current_value = current_country[value];
            var current_nameES_country = current_country['name']['es'];
            var current_nameEN_country = current_country['name']['en'];
            var option = create_option(current_value, current_nameES_country, null);

            if (i18n == true)
            option.dates = {date: {_id:current_country['_id'], 'code_i18n':current_country['code_i18n'],
                            name: {es:current_nameES_country, en:current_nameEN_country}}};
            else
                option.dates = {date: {_id:current_country['_id'],
                    name: {es:current_nameES_country, en:current_nameEN_country}}};

            contener.appendChild(option);
        }
    }
    function event_click_element (type_input, class_div, class_option, class_input, width_input, select, contener, link_add, path, value_json, who) {
        if (select.children.length != 0) {
            var option_selected = select.options[select.selectedIndex];
            var option_value = option_selected.value;
            var link_close = create_link_remove();
            link_close.value_option = option_value;
            link_close.dates = option_selected.dates;

            var cont_div = document.createElement('div');
            cont_div.setAttribute('class', class_div);
            cont_div.setAttribute('id', option_value);
            if (who == 'select') {
                var select_child = create_select(path+'.type_cut', option_selected.dates, '_id');
                var input = create_input(type_input, class_input, path+'.minutes', width_input);
                var i18n = false;

            } else {
                var tag_parent = create_label(class_option, null);
                var input = create_input(type_input, class_input, path+option_value, width_input);
                var i18n = true;
            }

            link_close.addEventListener('click', function(event){
                event.preventDefault();
                var value = this.value_option;
                var tag_content = document.getElementById(value);
                append_options_to_select(this.dates, value_json, select, i18n, link_add);
                tag_content.remove();
            }, false);

            if (class_input == 'input_titlelang') {
                var input_radio = create_input('radio', null, path+'original', null);
                input_radio.value = option_value;
                input_radio.setAttribute('required',"");
                input_radio.setAttribute('class','radio_titlelang_original');
                tag_parent.appendChild(input_radio);
                tag_parent.innerHTML += " ";
            }

            if (who == 'select') {
                cont_div.appendChild(select_child);
                cont_div.innerHTML += " ";
            } else {
                tag_parent.innerHTML += option_selected.innerHTML+" ";
                cont_div.appendChild(tag_parent);
            }

            cont_div.appendChild(input);
            cont_div.innerHTML += " ";
            cont_div.appendChild(link_close);
            contener.appendChild(cont_div);
            option_selected.remove();
            if (select.children.length == 0) {
                link_add.style.display = 'none';
                select.style.display = 'none';
            }
        }
    }

    // Functions called after the request ajax async
    function initial_TitleAndRelease_SelectTags (json) {
        all_countries_title_json = JSON.parse(JSON.stringify(json));
        all_idioms_json = JSON.parse(JSON.stringify(json));
        all_countries_json = JSON.parse(JSON.stringify(json));

        // removemos del json los que ya existent en el html por defecto
        remove_existent(titlelangs_default, all_idioms_json, 'code_i18n', 'name', true);
        remove_existent(releasecountries_default, all_countries_title_json, '_id', 'name', true);
        // Añadimos pues los paises restantes para añadir nuevo titulo a la pelicula
        append_options_to_select(all_idioms_json, 'code_i18n', select_new_title, true, new_title);
        append_options_to_select(all_countries_title_json, '_id', select_new_releasecountry, true, new_release);
        append_options_to_select(all_countries_json, '_id', select_new_country, false, null);

        // Eventos
        new_title.addEventListener('click', function(event){
            event_click_element('text', 'title_film', 'title_input','input_titlelang', '500px',
                select_new_title, contener_titlelangs, this, 'title.', 'code_i18n', 'input');
        }, false);
        new_release.addEventListener('click', function(event){
            event_click_element('date', 'release_film', 'title_input','input_releasecountry', '500px',
                select_new_releasecountry, contener_releasecountry, this, 'release.', '_id', 'input');
        }, false);
    }
    function initial_CutVersion_SelectTags (json) {
        all_cutversion_json = json;

        // removemos del json los que ya existent en el html por defecto
        remove_existent(cutversion_default, all_cutversion_json, '_id', 'value', false);
        // Añadimos pues los paises restantes para añadir nuevo titulo a la pelicula
        append_options_to_select(all_cutversion_json, '_id', select_new_cutversion, false, new_cut_version);

        // Eventos
        new_cut_version.addEventListener('click', function(event){
            var count_child = contener_cutversion.children.length;
            event_click_element('number', 'cutversion_film', 'option_cutversion','input_duration', '40px',
                select_new_cutversion, contener_cutversion, this, 'duration['+count_child+']', '_id', 'select');
        }, false);
    }
    
    
    /*
    * Edit ITEM
    */
    
    button_cancel_edit.onclick = function (event) {
        reset_form();
    };


    /*
    * Initial all SEARCH (ElasticSearch) inputs tag
    */

    var input_crew_writer_book = document.getElementById('book');
    var input_search_film = document.getElementById('search_film');

    var inputs_type_production = document.getElementsByClassName('checkbox_type_production');
    var inputs_others = document.getElementsByClassName('checkbox_others');
    var select_classification = document.getElementById('select_classification');
    var radio_title_original = document.getElementsByClassName('radio_titlelang_original');
    var input_year = document.getElementById('year');
    var checkbox_genres = document.getElementsByClassName('checkbox_genre');
    var checkbox_topics = document.getElementsByClassName('checkbox_topic');
    var input_website = document.getElementById('website');
    var textarea_sinopsis = document.getElementById('sinopsis');
    var input_trailer = document.getElementById('trailer');
    var cont_curiosities = document.getElementById('cont_curiosities');
    var input_cartel = document.getElementById('cartel');
    var divs_editable = document.getElementsByClassName('input_div_editable');


    // QUERIES
    function query_to_search_professional (text) {
        var json_query = '"fields":["artname","realname","_id"],"query":{"multi_match":{"query":"'+text+'","fields":["artname", "realname"],"fuzziness": 2,"prefix_length": 1,"analyzer":"simple"}}';
        return json_query;
    }
    function query_to_search_book (text) {
        var json_query = '"fields":["name.en","name.es","name.original","_id"],"query":{"multi_match":{"query":"'+text+'","fields":["name.*","_id"],"fuzziness": 2,"prefix_length": 1,"analyzer":"simple"}}';
        return json_query;
    }
    function query_to_search_film (text) {
        var json_query = '"query":{"multi_match":{"query":"'+text+'","fields":["title.*_*", "_id"],"fuzziness": 2,"prefix_length": 1,"analyzer":"simple"}}';
        return json_query;
    }

    // FUNCTIONS
    // reset form
    function reset_form () {
        remove_content_of_element(input_item_to_edit);
        id_item_to_delete.value = '';
        title_item_edit.innerHTML = '';
        contener_actions_edit.style.display = 'none';
        select_format.options[0].selected = true;
        select_category.options[0].selected = true;
        emitEvent(select_format, 'change');
        emitEvent(select_category, 'change');
        uncheck_all_checkbox(inputs_type_production);
        uncheck_all_checkbox(inputs_others);
        uncheck_all_checkbox(radio_title_original);
        select_classification.options[0].selected = true;
        // title, release and cutversion tags
        function remove_label_not_default (class_div, value_json, contener, select, send_i18n, link_add) {
            var divs_to_remove = [];
            var divs = contener.getElementsByClassName(class_div);
            for (var k=0;k<divs.length; k++) {
                var current = divs[k];
                if (!current.getAttribute('data-default')) {
                    divs_to_remove.push(current);
                } else {
                    // resetar campos por defecto
                    var inputs = current.getElementsByTagName('input');
                    for (var f=0; f<inputs.length; f++) {
                        var current_input = inputs[f];
                        if (current_input.getAttribute('class') == 'radio_titlelang_original')
                            current_input.checked = false;
                        else
                            current_input.value = '';
                    }
                }
            }
            for (var a=0;a<divs_to_remove.length; a++) {
                var div = divs_to_remove[a];
                var link_label_remove = div.getElementsByClassName('link')[0];
                append_options_to_select(link_label_remove.dates, value_json, select, send_i18n, link_add);
                div.parentNode.removeChild(div);
            }
        }
        remove_label_not_default('title_film', 'code_i18n', contener_titlelangs, select_new_title, true, new_title);
        remove_label_not_default('release_film', '_id', contener_releasecountry, select_new_releasecountry, true, new_release);
        remove_label_not_default('cutversion_film', '_id', contener_cutversion, select_new_cutversion, false, new_cut_version);
        switch_duration(cont_duration_cut, cont_duration_episode, input_cut, input_episode, true);
        // fin
        input_year.value = '';
        select_new_country.options[0].selected = true;
        for (var k=0; k<divs_editable.length; k++) {
            var current = divs_editable[k];
            var data_id = current.getAttribute('data-id');
            var contener_inputs_hidden = document.getElementById(data_id);
            remove_content_of_element(current);
            remove_content_of_element(contener_inputs_hidden);
        }
        uncheck_all_checkbox(checkbox_genres);
        uncheck_all_checkbox(checkbox_topics);
        input_website.value = '';
        textarea_sinopsis.value = '';
        input_trailer.value = '';
        input_cartel.value = '';
    }

    // fill suggest to edit
    function fill_suggest_clicked (suggest) {
        reset_form();
        var data = suggest.data;
        var default_num_genres = checkbox_genres.length;
        // title of film to edit in header and show buttons
        var text_title = '';
        if (data['title']['es_ES'])
            text_title = data['title']['es_ES'];
        else if (data['title']['en_US'])
            text_title = data['title']['en_US'];
        else {
            var i18n_original = data['title']['original'];
            text_title = data['title'][i18n_original];
        }
        title_item_edit.innerHTML = text_title;
        contener_actions_edit.style.display = 'block';
        // type format, category, production, others and classification
        var id_format = data['type']['format'];
        var category = data['type']['category'];
        choice_element_by(select_format.children, select_format, id_format, 'change');
        choice_element_by(select_category.children, select_category, category['_id'], 'change');
        if (category['style']) {
            var inputs_style = document.getElementsByClassName('input_style_category');
            choice_element_by(inputs_style, 'current', category['style'], 'change');
        }
        if (category['tecnics']) {
            var inputs_tecnic = document.getElementsByClassName('input_tecnic_category');
            choice_element_by(inputs_tecnic, null, category['tecnics'], null);
        }
        if (data['type']['production'])
            choice_element_by(inputs_type_production, null, data['type']['production'], null);
        if (data['others'])
            choice_element_by(inputs_others, null, data['others'], null);
        if (data['classification']) {
            var id_classification = data['classification'];
            choice_element_by(select_classification.children, select_classification, id_classification, 'change');
        }
        // titles
        var titles = data['title'];
        for (var i in titles) {
            var value = titles[i];
            var key = i;
            var get_class = 'input_titlelang';
            if (i == 'original') {
                key = value;
                get_class = 'radio_titlelang_original';
            }
            var cont_input_exists = document.getElementById(key);
            if (!cont_input_exists) {
                choice_element_by(select_new_title.children, select_new_title, key, 'change');
                emitEvent(new_title, 'click');
                cont_input_exists = document.getElementById(key);
            }
            var input = cont_input_exists.getElementsByClassName(get_class)[0];
            if (i == 'original')
                input.checked = true;
            else
                input.value = value;
        }
        // releases
        if (data['release']) {
            var releases = data['release'];
            var get_class = 'input_releasecountry';
            for (var a in releases) {
                var value = releases[a];
                var key = a;
                var cont_input_exists = document.getElementById(key);
                if (!cont_input_exists) {
                    choice_element_by(select_new_releasecountry.children, select_new_releasecountry, key, 'change');
                    emitEvent(new_release, 'click');
                    cont_input_exists = document.getElementById(key);
                }
                var input = cont_input_exists.getElementsByClassName(get_class)[0];
                input.value = value;
            }
        }
        // year
        input_year.value = data['year'];
        // duration
        if (data['duration']) {
            var duration = data['duration'];
            var get_class = 'input_duration';
            if (duration.hasOwnProperty('length')) {
                for (var f in duration) {
                    // it is a array, ie, movie
                    var array_cuts = duration[f];
                    var type_cut = array_cuts['type_cut'];
                    var minutes = array_cuts['minutes'];
                    var contener_exists = document.getElementById(type_cut);
                    if (!contener_exists) {
                        choice_element_by(select_new_cutversion.children, null, type_cut, null);
                        emitEvent(new_cut_version, 'click');
                        contener_exists = document.getElementById(type_cut);
                    }
                    var input = contener_exists.getElementsByClassName(get_class)[0];
                    input.value = minutes;
                }
            } else {
                // it is a serie or miniserie
                var input_text = document.getElementById('each_episode');
                input_text.value = duration['each_episode'];
            }
        }
        // country
        choice_element_by(select_new_country.children, null, data['country'], null);
        // crew
        var crew = data['crew'];
        for  (var c in crew) {
            var key = c;
            var values = crew[c];
            var div_editable = document.getElementById(key);
            for (var v=0; v<values.length; v++) {
                var current_value = values[v];
                var div_temp = create_div(current_value, null);
                if (key == 'book') {
                    get_item('"_id":"'+current_value+'"', 'books', get_text_from_json, div_temp);
                } else {
                    get_item('"_id":"'+current_value+'"', 'professionals', get_text_from_json, div_temp);
                }
                // this function is located in 'init_search_input_es.js'
                create_and_add_button_to_input(div_temp, div_editable);
            }
        }
        // generos
        /**
         * The specials genres are added after arrive to this part.
         * Because of this, We need a setInterval
         * for check that specials genres are added
         * */
        var genres_to_checked = [];
        if (data['genres']) {
            for (var i in data['genres']) {
                for (var g=0; g<data['genres'][i].length; g++) {
                    genres_to_checked.push(data['genres'][i][g]);
                }
            }
        }
        var genres_to_add = setInterval(function(){
            if (genres_to_checked.length != 0) {
                for (var a=0; a<genres_to_checked.length; a++) {
                    var genre_to_checked = genres_to_checked[a];
                    var checkbox_exists = document.getElementById(genre_to_checked);
                    if (checkbox_exists) {
                        checkbox_exists.checked = true;
                        genres_to_checked.splice(a, 1);
                    }
                }
            } else {
                console.log("parar interval");
                clearInterval(genres_to_add);
            }
        }, 100);
        // topics
        if (data['topics']) {
            var topics = data['topics'];
            for (var t=0; t<checkbox_topics.length; t++) {
                var current = checkbox_topics[t];
                for (var w=0; w<topics.length; w++) {
                    var topic = topics[w];
                    if (current.value == topic)
                        current.checked = true;
                }
            }
        }
        // website, sinopsis y trailer
        if (data['website'])
            input_website.value = data['website'];
        if (data['sinopsis'])
            textarea_sinopsis.value = data['sinopsis'];
        if (data['trailer'])
            input_trailer.value = data['trailer'];
        // append input hidden to 'edit contener'
        var id_input_hidden = document.createElement('input');
        id_input_hidden.setAttribute('type', 'hidden');
        id_input_hidden.setAttribute('name', 'edit');
        id_input_hidden.value = data['_id'];
        id_item_to_delete.value = data['_id'];
        input_item_to_edit.appendChild(id_input_hidden);

        // function utls for fill movie to edit
        function get_text_from_json (json, collection, element) {
            for (var a in json) {
                var buttons = document.getElementsByName(json[a]['_id']);
                for (var b=0; b<buttons.length; b++) {
                    var button = buttons[b];
                    if (json[a]['artname'])
                        button.value = json[a]['artname'];
                    else {
                        var code_lang = ['es_ES','es', 'en_US', 'en'];
                        for (var l=0;l<code_lang.length; l++) {
                            var lang = code_lang[l]
                            if (json[a]['name'][lang])
                                button.value = json[a]['name'][lang];
                        }
                    }
                }
            }
            //console.log(json[0]['dateborn']);
            //var date = json[0]['dateborn'];
            //console.log(new Date(date['$date']))
        }
    }

    function choice_element_by (content, element_to_emit, value_match, event) {
        for (var a=0; a<content.length; a++) {
            var current_item = content[a];
            // array with various values to match
            if (typeof value_match == 'object') {
                for (var f=0; f<value_match.length; f++) {
                    var current_value = value_match[f];
                    if (current_item.value == current_value) {
                        current_item.checked = true;
                        emitEvent(current_item, event);
                    }
                }
            } else {
                // only one value to match
                if (current_item.value == value_match) {
                    if (current_item.hasOwnProperty('selected'))
                        current_item.selected = true;
                    else
                        current_item.checked = true;
                    if (element_to_emit == 'current')
                        emitEvent(current_item, event);
                    else if (element_to_emit != null)
                        emitEvent(element_to_emit, event);
                }
            }
        }
    }


    // CALL FUNCTION
    // inputs 'crew'
    for (var x=0; x<inputs_crew.length; x++) {
        var current_element = inputs_crew[x];
        init_search_input_es('professionals', current_element, div_suggester, query_to_search_professional, 'artname', remove_textNodes, 'fields', false, true);
    }
    // input 'book' (in 'crew')
    init_search_input_es('books', input_crew_writer_book, div_suggester, query_to_search_book, 'name', remove_textNodes, 'fields',false, true);
    // input 'search film'
    init_search_input_es('films', input_search_film, div_suggester, query_to_search_film, 'title', remove_text, '_source', fill_suggest_clicked, false);


    /*
    * Initial CATEGORY select tag
    */

    var select_category = document.getElementById('select_category');
    var conteners_subcategories = document.getElementById('conteners_subcategories');
    var contener_tecnic = document.getElementById('contener_tecnic');
    var contener_style = document.getElementById('contener_style');
    var contener_inputs_tecnics = document.getElementById('contener_inputs_tecnics');
    var contener_inputs_styles = document.getElementById('contener_inputs_styles');
    var contener_specials_genres = document.getElementById('specials_genres');

    // FUNCTIONS for add options to 'select_category' tag
    function fill_category_select_tag (json, collection) {
        var cat_options = select_category.getElementsByTagName('option');
        for (var i=0; i<cat_options.length; i++) {
            var current_option = cat_options[i];
            if (current_option.value != ''){
                for (var j in json) {
                    if (json[j]['_id'] == current_option.value) {
                        check_tecnics_and_styles(json, 'tecnics');
                        check_tecnics_and_styles(json, 'styles');
                        break;
                    }
                }
            }
        }
        function check_tecnics_and_styles (json, name) {
            var tecnic_style = json[j][name];
            if (tecnic_style != undefined) {
                if (tecnic_style.hasOwnProperty('length')) {
                    if (tecnic_style.length > 0) {
                        // add a propierty to current_option
                        current_option[name] = tecnic_style;
                    }
                }
            }
        }
    }

    // FUNCTIONS for event 'onchange' of 'select_category' tag
    function print_specials_genres (json) {
        for (var f in json) {
            var current_special_genre = json[f];
            var type_genre = current_special_genre['class_type']['name'];
            var contener_type_genre = document.getElementById('contener_'+type_genre);
            if (contener_type_genre) {
                add_checkbox_special_genre(current_special_genre, contener_type_genre);
            } else {
                var label_title = document.createElement('label');
                var div_contener = document.createElement('div');
                var br = document.createElement('br');
                if (type_genre == 'demographic_genre') {
                    label_title.innerHTML = 'Demográficos';
                    div_contener.title_filed = 'demographics';
                } else if (type_genre == 'tematic_genre') {
                    label_title.innerHTML = 'Temáticos';
                    div_contener.title_filed = 'tematics';
                }
                label_title.setAttribute('class', 'title_specialgenre');
                div_contener.setAttribute('id', 'contener_'+type_genre);
                div_contener.appendChild(label_title);
                div_contener.appendChild(br);
                contener_specials_genres.appendChild(div_contener);
                add_checkbox_special_genre(current_special_genre, div_contener);
            }
        }
        function add_checkbox_special_genre (current, div_contener) {
            var name_special_genre = current['name'];
            var label_checkbox = document.createElement('label');
            var checkbox = document.createElement('input');
            label_checkbox.setAttribute('class', 'box_span_label');
            checkbox.setAttribute('type', 'checkbox');
            checkbox.setAttribute('class', 'checkbox_genre');
            checkbox.setAttribute('id', current['_id']);
            checkbox.setAttribute('data-type', div_contener.title_filed);
            checkbox.setAttribute('name', 'add.genres.'+div_contener.title_filed+'[]');
            checkbox.setAttribute('value', current['_id']);
            label_checkbox.innerHTML += " ";
            label_checkbox.appendChild(checkbox);
            if (name_special_genre['es'])
                label_checkbox.innerHTML += name_special_genre['es'];
            else
                label_checkbox.innerHTML += name_special_genre['en'];
            div_contener.appendChild(label_checkbox);
            div_contener.innerHTML += " ";
        }
    }
    function check_and_add_subcategories (option_selected, name, main_contener, contener_inputs, name_input) {
        if (option_selected[name]) {
            var opt = option_selected[name];
            main_contener.style.display = 'block';
            conteners_subcategories.style.display = 'block';
            for (var l in opt) {
                var current = opt[l];
                var label = document.createElement('label');
                var checkbox = document.createElement('input');
                checkbox.setAttribute('name', 'add.type.category.'+name_input);
                checkbox.setAttribute('value', current['_id']);
                if (name == 'styles') {
                    checkbox.setAttribute('class', 'input_style_category');
                    checkbox.setAttribute('type', 'radio');
                    label.style.fontSize = "12px";
                } else {
                    label.setAttribute('class', 'box_span_label');
                    checkbox.setAttribute('class', 'input_tecnic_category');
                    checkbox.setAttribute('type', 'checkbox');
                }
                label.appendChild(checkbox);
                if (current['name']['es'])
                    label.innerHTML += current['name']['es'];
                else
                    label.innerHTML += current['name']['en'];

                contener_inputs.appendChild(label);
                contener_inputs.innerHTML += " ";
            }
            if (name == 'styles' && opt.length > 0) {
                var set_style_radios = document.getElementsByClassName('input_style_category');
                for (var k=0; k<set_style_radios.length; k++) {
                    var current_radio = set_style_radios[k];
                    current_radio.onchange = function (event) {
                        remove_content_of_element(contener_specials_genres);
                        get_item('"class_type.style":"'+this.value+'"', 'FILM_GENRES_TOPICS', print_specials_genres);
                    }
                }
            }
        }
    }

    get_item('"type":"category"','FILM_FORMAT_CATEGORY_PRODUCTION', fill_category_select_tag);

    // Add event to select
    select_category.onchange = function (event) {
        var index_selected = event.target.selectedIndex;
        var option_selected = this.options[index_selected];
        remove_content_of_element(contener_inputs_tecnics);
        remove_content_of_element(contener_inputs_styles);
        remove_content_of_element(contener_specials_genres);
        contener_style.style.display = 'none';
        contener_tecnic.style.display = 'none';
        conteners_subcategories.style.display = 'none';
        check_and_add_subcategories(option_selected, 'styles', contener_style, contener_inputs_styles, 'style');
        check_and_add_subcategories(option_selected, 'tecnics', contener_tecnic, contener_inputs_tecnics, 'tecnics[]');
    }


    // FUNCTIONS UTILS

    // Create Elements

    function create_input (type, classname, name, width) {
        var input = document.createElement('input');
        input.setAttribute('type', type);
        if (classname)
            input.setAttribute('class', classname);
        input.setAttribute('name', 'add.'+name);
        input.style.width = width;
        return input;
    }
    function create_select (name, options, value) {
        var select = document.createElement('select');
        select.setAttribute('name', 'add.'+name);
        for (var i in options) {
            var current = options[i];
            var option = create_option(current[value], current['name']['es'], 'option_cutversion');
            select.appendChild(option);
        }
        return select;
    }
    function create_option (value, name_country, class_option) {
        var option = document.createElement('option');
        option.setAttribute('value', value);
        if (class_option != null)
            option.setAttribute('class', class_option);
        option.innerHTML = name_country;
        return option;
    }
    function create_label (classname, id) {
        var label = document.createElement('label');
        label.setAttribute('class', classname);
        if (id)
            label.setAttribute('id', id);
        return label;
    }
    function create_link_remove () {
        var a = document.createElement('a');
        a.setAttribute('href', '');
        a.setAttribute('class', 'link');
        a.innerHTML = '[x]';
        return a;
    }
    function create_div (id, classname) {
        var div = document.createElement('div');
        div.setAttribute('id', id);
        if (classname)
            div.setAttribute('class', classname);
        return div;
    }

    // Utils

    function remove_content_of_element (element) {
        while (element.lastChild) {
          element.removeChild(element.lastChild);
        }
    }
    function remove_textNodes () {
        for (var i=0; i<actual_input_focus.childNodes.length; i++) {
            var element = actual_input_focus.childNodes[i];
            if (element.nodeType == Node.TEXT_NODE) {
                element.remove();
                return true;
            }
        }
    }
    function remove_text () {
        input_search_film.value = '';
    }

    function emitEvent(element, type) {
        if ("createEvent" in document) {
            var event = document.createEvent("HTMLEvents");
            event.initEvent(type, false, true);
            element.dispatchEvent(event);
        }
        else
            element.fireEvent("on"+type);
        // reset value
    }

    function uncheck_all_checkbox (inputs) {
        for (var a=0; a<inputs.length; a++) {
            var current_item = inputs[a];
            current_item.checked = false;
        }
    }
}

// Comprobar si "document" está ya cargado (estaría en navegacion ajax)
// o, por lo contrario, añadir un listener para "window"

if (document.readyState == 'complete')
    film_loaded();
else
    window.addEventListener('load', film_loaded);