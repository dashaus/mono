function country_loaded () {

}

// Comprobar si "document" está ya cargado (estaría en navegacion ajax)
// o, por lo contrario, añadir un listener para "window"

if (document.readyState == 'complete') {
    country_loaded();
} else {
    window.addEventListener('load', country_loaded);
}