function classification_loaded () {
    function get_item (field, value) {
        var data_url = encodeURI("http://"+window.location.hostname+':27080/monoculum/FILM_CLASSIFICATION/_find?criteria='+'{"'+field+'":"'+value+'"}');
        console.log(data_url);
        var xhr = new XMLHttpRequest();

        xhr.onloadend = function() {
            if (xhr.status == 200) {
                var jsonObj = JSON.parse(xhr.responseText);
                if (jsonObj.results[0]) {
                    fill_form(jsonObj);
                    return true;
                } else {
                    console.log("no encontrado")
                    return false;
                }
            }
        };

        xhr.open("GET", data_url, true);
        xhr.send();
    }

    var links_edit = document.getElementsByName('link_edit');

    var name_es = document.getElementById('name_es');
    var name_en = document.getElementById('name_en');

    var contener_actions_edit = document.getElementById('contener_actions_edit');
    var title_from_edit = document.getElementById('title_from_edit');
    var div_edit_item = document.getElementById('edit_item_selected');
    var id_to_delete = document.getElementById('id_to_delete');


    /*
    Links para editar/eliminar
    */

    for (var i=0; i<links_edit.length; i++){
        var current_link = links_edit[i];
        current_link.addEventListener('click', function(event){
            event.preventDefault();
            get_item('_id', this.id);
        }, false);
    }

    function fill_form (json) {
        json = json.results[0];
        contener_actions_edit.style.display = 'inline';
        id_to_delete.value = json['_id'];
        if (json['name']['es'])
            title_from_edit.innerHTML = json['name']['es'];
        else
            title_from_edit.innerHTML = json['name']['en'];
        name_en.value = json['name']['en'];
        name_es.value = json['name']['es'];
        var input_edit_id = document.createElement('input');
        input_edit_id.setAttribute('type', 'hidden');
        input_edit_id.setAttribute('name', 'edit');
        input_edit_id.value = json['_id'];
        div_edit_item.appendChild(input_edit_id);
    }

    /*
    Botton to cancel selected edition
    */

    button_cancel_edit.addEventListener('click', function (event){
        reset_form();
    }, false);


    function reset_form () {
        remove_content_of_element(div_edit_item);
        title_from_edit.innerHTML = '';
        contener_actions_edit.style.display = 'none';
        name_en.value = '';
        name_es.value = '';
    }

    // Functions UTILS

    function remove_content_of_element (element) {
        while (element.lastChild) {
          element.removeChild(element.lastChild);
        }
    }
}

// Comprobar si "document" está ya cargado (estaría en navegacion ajax)
// o, por lo contrario, añadir un listener para "window"

if (document.readyState == 'complete') {
    classification_loaded();
} else {
    window.addEventListener('load', classification_loaded);
}