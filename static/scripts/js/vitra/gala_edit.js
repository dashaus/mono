function gala_loaded () {
    /*
     * Inicializar tags de select para country, city y borough
     */

    init_select_tags_to_country_cities_boroughs(false);


    /*
     * Inicializar tags de select para celebration
     */

    var select_each = document.getElementById('select_celebration_each');
    var select_month = document.getElementById('select_celebration_month');
    var value_celebration_month = document.getElementById('value_celebration_month');
    var value_celebration_each = document.getElementById('value_celebration_each');

    var value_country = document.getElementById('value_country');
    var value_city = document.getElementById('value_city');
    var value_borough = document.getElementById('value_borough');
    var select_country = document.getElementById('countries');
    var select_city = document.getElementById('cities');
    var select_borough = document.getElementById('boroughs');

    // SELECT COUNTRY, CITY, BOROUGH:
    // select the options that matches with 'value_calebration_month' and 'value_celebration_each'

    for (var i=0; i<select_each.options.length; i++) {
        var current_each = select_each.options[i];
        if (current_each.value == value_celebration_each.getAttribute('data-value')) {
            current_each.selected = true;
            break;
        }
    }
    for (var j=0; j<select_month.options.length; j++) {
        var current_month = select_month.options[j];
        if (current_month.value == value_celebration_month.getAttribute('data-value')) {
            current_month.selected = true;
            break;
        }
    }

    var country_item = value_country.getAttribute('data-value');
    var city_item = value_city.getAttribute('data-value');
    var borough_item = value_borough.getAttribute('data-value');
    find_option(country_item, select_country);
    emitEvent(select_country, 'change');
    if (city_item != '') {
        find_option(city_item, select_city);
        emitEvent(select_city, 'change');
        if (borough_item != '') {
            find_option(borough_item, select_borough);
            emitEvent(select_borough, 'change');
        }
    }

    value_country.remove();
    value_city.remove();
    value_borough.remove();


    // FUNCTIONS:
    // Necessaries for select tag 'country', 'city' and 'borough'

    function emitEvent(element, type) {
        if ("createEvent" in document) {
            var event = document.createEvent("HTMLEvents");
            event.initEvent(type, false, true);
            element.dispatchEvent(event);
        }
        else
            element.fireEvent("on"+type);
    }
    function find_option (value, select) {
        var get_options = select.getElementsByTagName('option');
        select.style.display = 'inline';
        for (var a=0; a<get_options.length; a++) {
            // evitamos comparar la opcion por defecto con value "0"
            if (a > 0) {
                var current_option = get_options[a];
                if (value == current_option.value) {
                    current_option.selected = true;
                    break;
                }
            }
        }
    }
}
// Comprobar si "document" está ya cargado (estaría en navegacion ajax)
// o, por lo contrario, añadir un listener para "window"

if (document.readyState == 'complete') {
    gala_loaded();
} else {
    window.addEventListener('load', gala_loaded);
}