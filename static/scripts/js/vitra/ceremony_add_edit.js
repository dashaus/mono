var json_loaded = null;
function check_year_ceremony (field, projection) {
    json_loaded = null;
    //var data_url = encodeURI("http://"+window.location.hostname+':27080/monoculum/users/_find?criteria='+'{"'+field+'":"'+value+'"}');
    var data_url = encodeURI("http://"+window.location.hostname+':27080/monoculum/galas/_find?criteria={'+field+'}&fields={'+projection+'}');
    var xhr = new XMLHttpRequest();

    xhr.onloadend = function() {
        if (xhr.status == 200) {
            var json_obj = JSON.parse(xhr.responseText);
            if (json_obj.results[0]) {
                console.log("la ceremonia si existe");
                json_loaded = json_obj;
                return json_obj;
            } else {
                console.log("la ceremonia no existe");
                json_loaded = false;
                return false;
            }
        }
    }

    xhr.open("GET", data_url, true);
    xhr.send();
}

function ceremony_loaded () {

    function uuid() {
        var uuid = "", i, random;
        for (i = 0; i < 32; i++) {
            random = Math.random() * 16 | 0;
            if (i == 8 || i == 12 || i == 16 || i == 20) {
                uuid += "-"
            }
            uuid += (i == 12 ? 4 : (i == 16 ? (random & 3 | 8) : random)).toString(16);
        }
        return uuid;
    }
    function hybrid_encode(str) {
        if (typeof(btoa) === 'function') {
            var uuid_base64 = window.btoa(encodeURIComponent(str)).slice(0,8)
        } else {
            console.log("¡El navegador no soporta la funcion btoa()!")
            console.log("Poner aqui una libreria Base64 para el navegador que no soporta btoa()")
        }
        return uuid_base64;
    }


    // 1.0 SECTIONS

    // 1.1 Get ELEMENTS from DOM
    var add_section = document.getElementById('section');
    var contener_section = document.getElementById('contener_input_section');
    var input_existent_section = document.getElementById('contener_section');
    var add_category = document.getElementById('category');
    var contener_category = document.getElementById('contener_input_category');
    var input_existent_category = document.getElementById('contener_category');
    var add_award = document.getElementById('award');
    var contener_award = document.getElementById('contener_input_award');
    var input_existent_award = document.getElementById('contener_award');

    var inputs_default = document.getElementsByClassName('input_default');

    var existents = document.getElementsByName('existent');

    // 1.2 Listener Events for ELEMENTS
    function click_event_contener(name, contener) {
        var create_uuid = hybrid_encode(uuid());
        // creamos elementos
        var input = document.createElement('input');
        // sus propiedades/atributos
        input.setAttribute('type', 'text');
        input.setAttribute('name', name);
        change_name_inputs(input, false);
        // añadimos al contenedor de 'sections'
        contener.appendChild(input);
    }

    add_section.addEventListener('click', function (event) {
        click_event_contener('section', contener_section);
    }, false);
    add_category.addEventListener('click', function (event) {
        click_event_contener('category', contener_category);
    }, false);
    add_award.addEventListener('click', function (event) {
        click_event_contener('award', contener_award);
    }, false);


    var sections_existent = input_existent_section.children.length;
    var categories_existent = input_existent_category.children.length;
    var awards_existent = input_existent_award.children.length;

    function change_name_inputs (inputs, contener) {
        function change (input) {
            var create_uuid = hybrid_encode(uuid());
            if (input.name == 'section') {
                input.name = 'section['+sections_existent+'].'+create_uuid+'.es';
                sections_existent++;
            } else if (input.name == 'category') {
                input.name = 'category['+categories_existent+'].'+create_uuid+'.es';
                categories_existent++;
            } else if (input.name == 'award') {
                input.name = 'award['+awards_existent+'].'+create_uuid+'.es';
                awards_existent++;
            }
        }

        if (contener == true) {
            for (var k=0; k<inputs.length; k++) {
                var input_default = inputs[k];
                change(input_default);
            }
        } else if (contener == false) {
            change(inputs);
        }
    }

    change_name_inputs(inputs_default, true);




    // 2.0 CEREMONIES

    // 2.1 Get ELEMENTS from DOM
    var years = document.getElementsByName("years");
    var title_year = document.getElementById("title_year");
    var input_year = document.getElementById('year');

    var sections = document.getElementById("sections");
    var links_sections = document.getElementsByName("link_section");
    var form_ceremony = document.getElementById("form");
    var inputs_added = document.getElementById("inputs");

    var current_gala = document.getElementsByName("gala")[0];
    var id_gala = current_gala.id;

    var categories = document.getElementById("categories");
    var awards = document.getElementById("awards");


    // ** YEARS **
    // 2.2 Listener Events for ELEMENTS

    // [FUNCTION event listener]
    var section = 0;
    var id_fs = 0;
    var year_selected = '';
    function click_event_year (event) {
        // reset the count in "section"
        section = 0;
        id_fs = 0;
        // rellenar titulo del año
        year_selected = event.target.innerHTML;
        title_year.innerHTML = year_selected;
        input_year.value = year_selected;
        // ocultar las secciones
        form_ceremony.setAttribute('class', 'hidden');
        sections.setAttribute('class', 'sections_available');
        for (var s = 0; s<links_sections.length; s++) {
            var section_json_link = links_sections[s];
            section_json_link.style.display = 'inline-block';
        }
        var parent_inputs = inputs_added.parentNode;
        inputs_added.remove();
        inputs_added = document.createElement('div');
        inputs_added.setAttribute('id','inputs');
        parent_inputs.insertBefore(inputs_added, parent_inputs[0]);

        var find = '"ceremonies.year":'+'"'+year_selected+'"';
        var projection = '"ceremonies.$":1';
        check_year_ceremony('"_id":"'+id_gala+'",'+find, projection);
        var interval = setInterval(function(){
            if (json_loaded != null) {
                clearInterval(interval);
                if (json_loaded != false) {
                    var ceremony = json_loaded['results'][0]['ceremonies'][0];
                    var sections_ceremony = ceremony['sections'];
                    for (var a = 0; a<sections_ceremony.length; a++) {
                        for (var sec = 0; sec<links_sections.length; sec++) {
                            var section_json = sections_ceremony[a];
                            var id_section_json = section_json['id_section'];
                            var position_section_json = section_json['position'];
                            var deep_section_json = section_json['deep'];
                            var link_section = links_sections[sec];
                            if (id_section_json == link_section.id) {
                                console.log("sec: ", a);
                                section_json.deep = section_json['deep'];

                                // creamos un elemento temporal para pasarselo a la funcion
                                // create_categories_awards() como primer argumento
                                var section_to_add = document.createElement('span');
                                // añadimos al elemento temporal el id y nombre de ruta 'json'
                                section_to_add.id = id_section_json;
                                section_to_add.name_path = 'ceremony.sections['+a+']';
                                section_to_add.deep = deep_section_json;
                                section_to_add.position = position_section_json;
                                section_to_add.innerHTML = link_section.innerHTML;
                                create_categories_awards(section_to_add, inputs_added, 'fieldset_section', '.id_section', true, section_json);

                                // /eliminamos el link "section" ya creado en la ceremonia
                                link_section.style.display = 'none';
                            }
                        }
                    }
                } else {
                    //console.log("No existe, majete.")
                }
            }
        }, 100);
    }

    // [ADD event listener]
    for (var y = 0; y<years.length; y++) {
        var link_year = years[y];
        link_year.addEventListener('click', function (event){
            event.preventDefault();
            click_event_year(event);
        }, false);
    }
    for (var s = 0; s<links_sections.length; s++) {
        var section_json_link = links_sections[s];
        section_json_link.addEventListener('click', function (event) {
            // comprobar si hay secciones ya insertadas
            // (se suele utilizar cuando se edita/actualiza una ceremonia ya existente)
            var count_sections = form_ceremony.getElementsByClassName('fieldset_section');
            section = count_sections.length;

            while (document.getElementById(id_fs) != null){
              id_fs++;
            }

            this.name_path = 'ceremony.sections['+id_fs+']';
            this.deep = id_fs.toString();
            this.position = section;

            create_categories_awards(this, inputs_added, 'fieldset_section', '.id_section', false, null);
            event.target.style.display = 'none';
            section++;
        }, false);
    }

    // ** SECTIONS **
    // 2.2 Listener Events for ELEMENTS

    // [FUNCTION event listener] (1)
    function create_categories_awards (section, contener, class_element, type_id, ceremony_exists, ceremony) {
        form_ceremony.setAttribute('class', 'form_available');

        var pos_div_fieldset = '';
        var fieldset = '';
        var fieldset_legend = '';
        var fieldset_legend_a = '';
        var fieldset_legend_input_position = '';
        var fieldset_div = '';
        var fieldset_categories = '';
        var fieldset_awards = '';
        var fieldset_hidden_idsection = '';
        var fieldset_hidden_deep = '';

        var fieldset_categories_link = '';
        var fieldset_awards_link = '';
        var fieldset_awards_div_inputs = '';
        var fieldset_category_div_categories = '';

        function create_section_or_category (section, contener, class_element, type_id) {
            var position = '';
            var num_position = section.position;
            // creamos elementos
            pos_div_fieldset = document.createElement('div');
            fieldset = document.createElement('fieldset');
            fieldset_legend = document.createElement('legend');
            fieldset_legend_a = document.createElement('a');
            fieldset_legend_input_position = document.createElement('input');
            fieldset_div = document.createElement('div');
            fieldset_categories = categories.cloneNode(true);
            fieldset_awards = awards.cloneNode(true);
            fieldset_category_div_categories = document.createElement('div');
            fieldset_awards_div_inputs = document.createElement('div');
            fieldset_hidden_idsection = document.createElement('input');
            fieldset_hidden_deep = document.createElement('input');
            //atributos
            fieldset.setAttribute('class', class_element);
            if (section.deep.length > 1) {
                // el fieldset es una categoria ya que la profundidad es mayor a 1
                var parent_deep = section.deep.slice(0, -1);
                position = parent_deep+'_pos_cat_'+num_position;
                fieldset.name = 'contener_cat_'+parent_deep;
                pos_div_fieldset.id = 'wrap_fs_'+parent_deep+'_pos_'+num_position;
            } else {
                // el fieldset es una seccion ya que solo hay una profundidad
                position = 'pos_sec_'+num_position;
                fieldset.name = 'contener_sec';
                pos_div_fieldset.id = 'wrap_fs_pos_'+num_position;
            }
            fieldset.id = section.deep;
            fieldset_legend.innerHTML = section.innerHTML;
            fieldset_legend_a.setAttribute('href', '');
            fieldset_legend_a.setAttribute('class', 'remove');
            fieldset_legend_a.position = num_position;
            fieldset_legend_a.innerHTML = '[x]';
            fieldset_legend_input_position.setAttribute('type', 'text');
            fieldset_legend_input_position.setAttribute('name', section.name_path+'.position');
            fieldset_legend_input_position.setAttribute('id', position);
            fieldset_legend_input_position.setAttribute('class', 'position');
            fieldset_legend_input_position.value = num_position;
            fieldset_legend_input_position.int = num_position;
            fieldset_legend_input_position.id_contener = section.deep;
            fieldset_div.setAttribute('class', 'wrap_categories_awards');
            fieldset_awards_div_inputs.setAttribute('class', 'awards_inputs');
            fieldset_category_div_categories.setAttribute('class', 'contener_categories');
            fieldset_categories.setAttribute('class', 'catgs_links_available');
            fieldset_awards.setAttribute('class', 'awds_links_available');
            var links_categories = fieldset_categories.getElementsByClassName('link');
            for (var v=0; v<links_categories.length; v++) {
                var link_cat = links_categories[v];
                link_cat.name = 'links_cat_'+section.deep;
            }
            var links_awards = fieldset_awards.getElementsByClassName('link');
            for (var d=0; d<links_awards.length; d++) {
                var link_awd = links_awards[d];
                link_awd.name = 'links_awd_'+section.deep;
            }
            fieldset_categories.id = 'cat_'+section.deep;
            fieldset_awards.id = 'awd_'+section.deep;
            fieldset_hidden_idsection.setAttribute('type', 'hidden');
            fieldset_hidden_idsection.setAttribute('name', section.name_path+type_id);
            fieldset_hidden_idsection.setAttribute('id', 'id_'+section.deep);
            fieldset_hidden_idsection.value = section.id;
            fieldset_hidden_deep.setAttribute('type', 'hidden');
            fieldset_hidden_deep.setAttribute('name', section.name_path+'.deep');
            fieldset_hidden_deep.setAttribute('id', 'deep_'+section.deep);
            fieldset_hidden_deep.value = section.deep;
            //embeber
            fieldset_awards.appendChild(fieldset_awards_div_inputs);
            fieldset_categories.appendChild(fieldset_category_div_categories);
            fieldset_div.appendChild(fieldset_categories);
            fieldset_div.appendChild(fieldset_awards);
            fieldset_legend.appendChild(fieldset_legend_a);
            fieldset_legend.appendChild(fieldset_legend_input_position);
            fieldset.appendChild(fieldset_legend);
            fieldset.appendChild(fieldset_div);
            fieldset.appendChild(fieldset_hidden_idsection);
            fieldset.appendChild(fieldset_hidden_deep);
            pos_div_fieldset.appendChild(fieldset);
            contener.appendChild(pos_div_fieldset);


            fieldset_categories_link = document.getElementsByName('links_cat_'+section.deep);
            fieldset_awards_link = document.getElementsByName('links_awd_'+section.deep);

            // [ADD event listener]
            var category = 0;
            for (var c = 0; c<fieldset_categories_link.length; c++) {
                var current_category_link = fieldset_categories_link[c];
                current_category_link.addEventListener('click', function (event) {
                    event.preventDefault();
                    var id_cat = 0;
                    this.name_path = section.name_path;
                    this.deep = section.deep;

                    var links_to_remove = document.getElementById('awd_'+this.deep);
                    if (links_to_remove != null) {
                        links_to_remove.style.display = 'none';
                    }
                    var parent_deep_category = section.deep;
                    var parent_contener = document.getElementById(parent_deep_category);
                    var fieldsets_contener = document.getElementsByName('contener_cat_'+parent_deep_category);
                    var contener = parent_contener.getElementsByClassName('contener_categories')[0];

                    console.log(this.deep);
                    while (document.getElementById(this.deep+id_cat) != null){
                      id_cat++;
                    }

                    category = fieldsets_contener.length;
                    this.name_path += '.categories['+id_cat+']';
                    this.deep += id_cat;
                    this.position = category;
                    click_event_category(this, contener);
                    category++;
                }, false);
            }
            var award = 0;
            for (var a = 0; a<fieldset_awards_link.length; a++) {
                var current_awards_link = fieldset_awards_link[a];
                current_awards_link.addEventListener('click', function (event) {
                    event.preventDefault();
                    var id_awd = 0;
                    this.name_path = section.name_path;
                    this.deep = section.deep;

                    var links_to_remove = document.getElementById('cat_'+this.deep);
                    if (links_to_remove != null) {
                        links_to_remove.style.display = 'none';
                    }
                    var contener = this.parentNode.getElementsByClassName('awards_inputs')[0];
                    var count_awards = document.getElementsByName('contener_awd_'+section.deep);

                    while (document.getElementById(this.deep+id_awd) != null){
                      id_awd++;
                    }

                    award = count_awards.length;
                    this.name_path += '.awards['+id_awd+']';
                    this.deep += ''+id_awd;
                    this.position = award;
                    click_event_award(this, this.parentNode, '');
                    award++;
                }, false);
            }

            fieldset_legend_a.addEventListener('click', function (event) {
                event.preventDefault();
                if (section.deep.length > 1) {
                    var who_is = 'cat';
                } else {
                    var who_is = 'sec';
                }
                click_event_remove(this, section, who_is, 'fs');
            }, false);

            fieldset_legend_input_position.addEventListener('keyup', function (event) {
                var match = event.target.id.match(/(^\d+)/);
                if (match != null) {
                    // es un 'cat'
                    var who_is = 'cat';
                    var prefix = 'contener_cat_';
                } else {
                    var who_is = 'sec';
                    var prefix = 'contener_sec';
                }
                keyup_event(event, match, this, prefix, who_is, 'fs')
            }, false);
        }

        create_section_or_category(section, contener, class_element, type_id);

        if (ceremony_exists == false) {
            // PRIMER PASO
            // Crear la sección clicada por el usuario
        } else if (ceremony_exists == true) {
            // PRIMER PASO
            // Crear las secciones de la ceremonia existente
            function recursive_create_category_or_award (ceremony) {
                if (ceremony['categories']) {
                    var categories = ceremony['categories'];
                    for (cat in categories) {
                        categories = ceremony['categories'][cat];
                        var cat_name_path = section.name_path;
                        var id_category = categories['id_category'];
                        var position_category = categories['position'];

                        var deep_category = categories['deep'];
                        var deep_category_without_section = deep_category.slice(1);
                        var parent_deep_category = deep_category.slice(0,-1);

                        // cojemos los links 'categories' de la categoria
                        // para crear el name_path, crear a categoria y eliminar su
                        // respectivo link
                        var fs_parent_category = document.getElementById(parent_deep_category);
                        var contener = fs_parent_category.getElementsByClassName('contener_categories')[0];
                        var links_parent_category = document.getElementsByName('links_cat_'+parent_deep_category);
                        for (var lcat = 0; lcat<links_parent_category.length; lcat++) {
                            var link_category = links_parent_category[lcat];
                            if (id_category == link_category.id) {
                                link_category.deep = deep_category;
                                link_category.position = position_category;
                                // calculmos la ruta de 'categories' mediante su 'deep'
                                for (var dl=0; dl<deep_category_without_section.length; dl++) {
                                    var actual_deep = deep_category_without_section[dl];
                                    cat_name_path += '.categories['+actual_deep+']';
                                }
                                link_category.name_path = cat_name_path;
                                click_event_category(link_category, contener);
                            }
                        }
                        // cojemos los links padre 'awards' de la categoria
                        // para eliminarlos
                        var links_awds_to_remove = document.getElementById('awd_'+parent_deep_category);
                        if (links_awds_to_remove != null) {
                            // es decir, no ha sido borrado ya
                            links_awds_to_remove.style.display = 'none';
                        }
                        //vamos al siguiente nivel de profundidad (si hay)
                        recursive_create_category_or_award(categories);
                    }
                } else if (ceremony['awards']) {
                    var awards = ceremony['awards'];
                    var deep_category_awards = ceremony['deep'];
                    var contener = document.getElementById(deep_category_awards);
                    for (awd in awards) {
                        var award = ceremony['awards'][awd];
                        var id_award = award['id_award'];
                        var value_award = award['film'];
                        var position_award = award['position'];
                        var awd_name_path = section.name_path;

                        var deep_awards = award['deep'];
                        var deep_awards_without_section = deep_awards.slice(1);

                        var links_parent_award = document.getElementsByName('links_awd_'+deep_category_awards);
                        for (var lawd = 0; lawd<links_parent_award.length; lawd++) {
                            var link_award = links_parent_award[lawd];
                            if (id_award == link_award.id) {
                                link_award.deep = deep_awards;
                                link_award.position = position_award;
                                // calculmos la ruta de 'categories' mediante su 'deep'
                                for (var da=0; da<deep_awards_without_section.length; da++) {
                                    var actual_deep = deep_awards_without_section[da];
                                    if (da == deep_awards_without_section.length-1) {
                                        awd_name_path += '.awards['+actual_deep+']';
                                    } else {
                                        awd_name_path += '.categories['+actual_deep+']';
                                    }
                                }
                                link_award.name_path = awd_name_path;
                                click_event_award(link_award, contener, value_award);
                            }
                        }
                        // cojemos los links padre 'categories' de la categoria
                        // para eliminarlos
                        var links_to_remove = document.getElementById('cat_'+deep_category_awards);
                        if (links_to_remove != null) {
                            // es decir, no ha sido ocultado ya
                            links_to_remove.style.display = 'none';
                        }
                    }
                }
            }
            recursive_create_category_or_award(ceremony);
        }

        // [FUNCTION event listener] (2)
        function click_event_remove (link_clicked, item, who_is, wrap_name) {
            var item_deep = item.deep;
            var item_position = link_clicked.position;

            if (who_is == 'sec') {
                var parent_deep = item_deep;
                var links = 'link_section';
                var token_to_remove = document.getElementById('wrap_'+wrap_name+'_pos_'+item_position);
                var items = document.getElementsByName('contener_'+who_is);
            } else {
                // entonces será o 'cat' o 'awd'
                var parent_deep = item_deep.slice(0,-1);
                var links = 'links_'+who_is+'_'+parent_deep;
                var token_to_remove = document.getElementById('wrap_'+wrap_name+'_'+parent_deep+'_pos_'+item_position);
                var items = document.getElementsByName('contener_'+who_is+'_'+parent_deep);
            }

            //var token_to_remove = document.getElementById(item_deep);
            var id_contener_to_remove = document.getElementById('id_'+item_deep);
            var content_parent_links = document.getElementsByName(links);
            for (var la=0; la<content_parent_links.length; la++) {
                var current_link = content_parent_links[la];
                if (id_contener_to_remove.value == current_link.id) {
                    current_link.style.display = 'inline-block';
                    token_to_remove.remove();

                    var count_items = items.length;
                    if (count_items == 0) {
                        if (who_is == 'cat') {
                            var links_awds = document.getElementById('awd_'+parent_deep);
                            links_awds.style.display = 'compact';
                        } else if (who_is == 'awd') {
                            var links_cats = document.getElementById('cat_'+parent_deep);
                            links_cats.style.display = 'compact';
                        }
                        console.log("estoy soooolooo. Nooooo", parent_deep, who_is);
                    } else {
                        for (var a = 0; a<count_items; a++) {
                            var current_item = items[a];
                            var position_item = current_item.getElementsByClassName('position')[0];
                            if (position_item.value != a) {
                                change_position(parent_deep, position_item, a, who_is, wrap_name);
                            }
                        }
                    }
                }
            }

        }
        function keyup_event (event, match, current, prefix_contener, who_is, wrap_name) {
            var key_pressed = event.target.value;
            var this_int = current.int;

            if (match != null) {
                // es un 'cat' o 'awd'
                var parent_deep = match[0];
                var conteners = prefix_contener+parent_deep;
                var id_input_original = parent_deep+'_pos_'+who_is+'_'+this_int;
                var id_input_swap_original = parent_deep+'_pos_'+who_is+'_'+key_pressed;
            } else {
                var conteners = prefix_contener;
                var id_input_original = 'pos_'+who_is+'_'+this_int;
                var id_input_swap_original = 'pos_'+who_is+'_'+key_pressed;
            }

            var fieldsets_available = document.getElementsByName(conteners);
            for (var k=0; k<fieldsets_available.length; k++) {
                if (key_pressed == k.toString()) {
                    var input_original = document.getElementById(id_input_original);
                    var input_swap_original = document.getElementById(id_input_swap_original);
                    swap_id_and_position(parent_deep, input_original, input_swap_original, who_is, wrap_name);
                    break;
                } else if (key_pressed == '') {

                } else {
                    current.value = this_int;
                }
            }
        }

        function click_event_category (category, contener) {
            create_section_or_category(category, contener, 'fieldset_category', '.id_category');
            console.log(category.name_path);
            category.style.display = 'none';
        }
        function click_event_award (award_choiced, fieldset_awards, value_award) {
            var contener = fieldset_awards.getElementsByClassName('awards_inputs')[0];
            var parent_deep = award_choiced.deep.slice(0,-1);
            var num_position = award_choiced.position;
            var position = parent_deep+'_pos_awd_'+num_position;

            //premio seleccionado
            var text_award_choiced = award_choiced.innerHTML;
            // create elements

            var div_input = document.createElement('div');
            var label_award = document.createElement('label');
            var span_award = document.createElement('span');
            var link_remove_a = document.createElement('a');
            var input_award = document.createElement('input');
            var input_position = document.createElement('input');
            var input_hidden_idaward = document.createElement('input');
            var input_hidden_deep = document.createElement('input');
            var newline = document.createElement('br');
            // atributos para elements
            div_input.id = 'wrap_awd_'+parent_deep+'_pos_'+num_position;
            span_award.innerHTML = text_award_choiced+" ";
            span_award.setAttribute('class', 'span_label');
            label_award.setAttribute('name', 'contener_awd_'+parent_deep);
            label_award.setAttribute('id', award_choiced.deep);
            input_award.setAttribute('type', 'text');
            input_award.setAttribute('name', award_choiced.name_path+'.film');
            input_award.value = value_award;
            link_remove_a.setAttribute('href', '');
            link_remove_a.setAttribute('class', 'remove');
            link_remove_a.position = award_choiced.position;
            link_remove_a.innerHTML = '[x]';
            input_hidden_idaward.setAttribute('type', 'hidden');
            input_hidden_idaward.setAttribute('name', award_choiced.name_path+'.id_award');
            input_hidden_idaward.id = 'id_'+award_choiced.deep;
            input_hidden_idaward.value = award_choiced.id;
            input_hidden_deep.setAttribute('type', 'hidden');
            input_hidden_deep.setAttribute('name', award_choiced.name_path+'.deep');
            input_hidden_deep.value = award_choiced.deep;
            input_position.setAttribute('type', 'text');
            input_position.setAttribute('name', award_choiced.name_path+'.position');
            input_position.setAttribute('id', position);
            input_position.setAttribute('class', 'position');
            input_position.value = num_position;
            input_position.int = num_position;
            input_position.id_contener = award_choiced.deep;
            //embeber
            label_award.appendChild(span_award);
            label_award.appendChild(input_award);
            label_award.appendChild(input_position);
            label_award.appendChild(link_remove_a);
            label_award.appendChild(input_hidden_deep);
            label_award.appendChild(input_hidden_idaward);
            label_award.appendChild(newline);
            div_input.appendChild(label_award);
            contener.appendChild(div_input);

            award_choiced.style.display = 'none';

            link_remove_a.addEventListener('click', function (event) {
                event.preventDefault();
                click_event_remove(this, award_choiced, 'awd', 'awd');
            }, false);

            input_position.addEventListener('keyup', function (event) {
                var match = event.target.id.match(/(^\d+)/);
                keyup_event(event, match, this, 'contener_awd_', 'awd', 'awd')
            }, false);
        }



        // Helpers
        function swap_id_and_position (parent_deep, input_position1, input_position2, who_is, wrap_name) {
            var position1 = input_position1.int;
            var position2 = input_position2.int;
            var contener_input1 = document.getElementById(input_position1.id_contener);
            var contener_input2 = document.getElementById(input_position2.id_contener);

            if (who_is == 'cat' || who_is == 'awd') {
                var wrap_position1 = document.getElementById('wrap_'+wrap_name+'_'+parent_deep+'_pos_'+position1);
                var wrap_position2 = document.getElementById('wrap_'+wrap_name+'_'+parent_deep+'_pos_'+position2);
                input_position1.id = parent_deep+'_pos_'+who_is+'_'+position2;
                input_position2.id = parent_deep+'_pos_'+who_is+'_'+position1;
            } else if (who_is == 'sec') {
                // el wrap es, ligeramente, diferente para secciones
                var wrap_position1 = document.getElementById('wrap_'+wrap_name+'_pos_'+position1);
                var wrap_position2 = document.getElementById('wrap_'+wrap_name+'_pos_'+position2);
                input_position1.id = 'pos_'+who_is+'_'+position2;
                input_position2.id = 'pos_'+who_is+'_'+position1;
            }

            var link_remove_input1 = wrap_position1.getElementsByClassName('remove')[0];
            var link_remove_input2 = wrap_position2.getElementsByClassName('remove')[0];

            link_remove_input1.position = position2;
            link_remove_input2.position = position1;
            input_position1.value = position2;
            input_position2.value = position1;
            input_position1.int = position2;
            input_position2.int = position1;

            wrap_position1.appendChild(contener_input2);
            wrap_position2.appendChild(contener_input1);
        }

        function change_position (parent_deep, input_position, new_position_to_change, who_is, wrap_name) {

            if (who_is == 'cat' || who_is == 'awd') {
                var wrap_position_of_input = document.getElementById('wrap_'+wrap_name+'_'+parent_deep+'_pos_'+input_position.int);
                input_position.id = parent_deep+'_pos_'+who_is+'_'+new_position_to_change;
                wrap_position_of_input.id = 'wrap_'+wrap_name+'_'+parent_deep+'_pos_'+new_position_to_change;
            } else if (who_is == 'sec') {
                var wrap_position_of_input = document.getElementById('wrap_fs_pos_'+input_position.int);
                input_position.id = 'pos_'+new_position_to_change;
                wrap_position_of_input.id = 'wrap_fs_pos_'+new_position_to_change;
            }

            var link_remove = wrap_position_of_input.getElementsByClassName('remove')[0];

            link_remove.position = new_position_to_change;
            input_position.int = new_position_to_change;
            input_position.value = new_position_to_change;
        }

    }

}

// Comprobar si "document" está ya cargado (estaría en navegacion ajax)
// o, por lo contrario, añadir un listener para "window"

if (document.readyState == 'complete') {
    ceremony_loaded();
} else {
    window.addEventListener('load', ceremony_loaded);
}