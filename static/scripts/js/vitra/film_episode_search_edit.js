function filmEpisodeLoaded () {

    var urlManageEpisode = document.getElementById('urlManageEpisode');
    var URL_FORMAT = urlManageEpisode.innerHTML;
    urlManageEpisode.remove();
    var containerNewSeason = document.getElementById('containerNewSeason');
    var containerSeasonsAdded = document.getElementById('containerSeasonsAdded');

    /*
     * INITIALIZE THE SEARCH INPUT TO CHOICE THE FILM TO ADD EPISODE
     */

    var inputSearch = document.getElementById('searchItem');
    var divSuggester = document.getElementById('suggester');
    var inputItemToEdit = document.getElementById('inputItemToEdit');

    function queryToSearch (text) {
        var json_query = '"fields":["title.en_US","title.es_ES","title.en_GB","title.es_MX", "_id"],"query":{"multi_match":{"query":"'+text+'","fields":["title.*_*", "_id"],"fuzziness": 2,"prefix_length": 1,"analyzer":"simple"}},"filter":{"query":{"bool":{"should":[{"match":{"type.format":"AwADAniQ"}},{"match":{"type.format":"VH6zEGzG"}}]}}}';
        return json_query;
    }
    function clickSuggest (suggest) {
        var id = suggest.data['_id'];
        var finalURL = URL_FORMAT.replace(/id_film_here/, id);
        if (location.href != finalURL) {
            loadAndChange_URL_AJAX(finalURL, null);
        }
    }

    // initial input
    init_search_input_es('films', inputSearch, divSuggester,
        queryToSearch, 'title', removeTextNodes, 'fields', clickSuggest, false);


    /*
    * ADD NEW SEASON
    */




    /*
     * FUNCTION UTILS
     */

    function removeTextNodes () {
        inputSearch.value = '';
    }
    function emitEvent(element, type) {
        if ("createEvent" in document) {
            var event = document.createEvent("HTMLEvents");
            event.initEvent(type, false, true);
            element.dispatchEvent(event);
        }
        else
            element.fireEvent("on"+type);
    }
    function removeContentOfElement (element, all) {
        if (all == true) {
            while (element.lastChild) {
              element.removeChild(element.lastChild);
            }
        } else {
            while (element.lastChild && element.children.length > 1) {
              element.removeChild(element.lastChild);
            }
        }
    }
    function uncheckAll (contener) {
        var checkboxs = contener.getElementsByTagName('input');
        for (var i=0; i<checkboxs.length;i++) {
            var current_checkbox = checkboxs[i];
            current_checkbox.checked = false;
        }
    }
    function resetForm () {
        removeContentOfElement(inputItemToEdit, true);
    }
}



// Comprobar si "document" está ya cargado (estaría en navegacion ajax)
// o, por lo contrario, añadir un listener para "window"

if (document.readyState == 'complete')
    filmEpisodeLoaded();
else
    window.addEventListener('load', filmEpisodeLoaded);