function genre_loaded () {
    function data_exists (field, value) {
        var data_url = encodeURI("http://"+window.location.hostname+':27080/monoculum/FILM_GENRES_TOPICS/_find?criteria='+'{"'+field+'":"'+value+'"}');
        var xhr = new XMLHttpRequest();

        xhr.onloadend = function() {
            if (xhr.status == 200) {
                var jsonObj = JSON.parse(xhr.responseText);
                if (jsonObj.results[0]) {
                    fill_genre_topic(jsonObj);
                    return true;
                } else {
                    return false;
                }
            }
        };

        xhr.open("GET", data_url, true);
        xhr.send();
    }

    var links_genres_edit = document.getElementsByName('genres_edit');
    var links_topics_edit = document.getElementsByName('topics_edit');

    var radio_genre = document.getElementById('radio_genre');
    var radio_topic = document.getElementById('radio_topic');
    var name_es = document.getElementById('name_es');
    var name_en = document.getElementById('name_en');
    var description_es = document.getElementById('description_es');
    var description_en = document.getElementById('description_en');

    var div_edit_item = document.getElementById('edit_item_selected');
    var title_from_edit = document.getElementById('title_from_edit');
    var button_cancel_edit = document.getElementById('button_cancel_edit');
    var contener_actions_edit = document.getElementById('contener_actions_edit');
    var id_to_delete = document.getElementById('id_to_delete');

    var radio_type = document.getElementsByName('add.type');
    var aditional_options = document.getElementById('aditional_options');
    var select_style_category = document.getElementById('select_style_category');
    var contener_types_genre = document.getElementById('contener_types_genre');
    var radio_types_genre = document.getElementsByName('add.class_type.name');


    /*
    Links para editar/eliminar
    */

    for (var i=0; i<links_genres_edit.length; i++){
        var link_genre = links_genres_edit[i];
        link_genre.addEventListener('click', function(event){
            event.preventDefault();
            data_exists('_id', this.id)
        }, false);
    }
    for (var i=0; i<links_topics_edit.length; i++){
        var link_topic = links_topics_edit[i];
        link_topic.addEventListener('click', function(event){
            event.preventDefault();
            data_exists('_id', this.id)
        }, false);
    }

    function fill_genre_topic (json) {
        json = json.results[0];
        var radio_to_check = document.getElementById('radio_'+json['type']);
        radio_to_check.checked = true;

        name_es.value = ''; // tal vez, no haya un nombre para 'es'...
        for (var x in json['name']) {
            var name = json['name'][x];
            var input = document.getElementById('name_'+x);
            input.value = name;
        }
        for (var x in json['description']) {
            var name = json['description'][x];
            var textarea = document.getElementById('description_'+x);
            textarea.value = name;
        }
        if (json['class_type']) {
            var class_type = json['class_type'];
            for (var o=0; o<select_style_category.length; o++) {
                var current_option = select_style_category[o];
                if (current_option.value == class_type['style']) {
                    aditional_options.style.display = 'block';
                    current_option.selected = true;
                    emitEvent(select_style_category, 'change');
                    var radio_to_select = document.getElementById('radio_'+class_type['name']);
                    radio_to_select.checked = true;
                }
            }
        } else {
            reset_type_genre(json['type']);
        }

        contener_actions_edit.style.display = 'inline';
        id_to_delete.value = json['_id'];
        if (json['name']['es'])
            title_from_edit.innerHTML = json['name']['es'];
        else
            title_from_edit.innerHTML = json['name']['en'];
        var input_edit_id = document.createElement('input');
        input_edit_id.setAttribute('type', 'hidden');
        input_edit_id.setAttribute('name', 'edit');
        input_edit_id.value = json['_id'];
        div_edit_item.appendChild(input_edit_id);
    }

    /*
    Botton to cancel selected edition
    */

    button_cancel_edit.addEventListener('click', function (event){
        reset_form();
    }, false);




    /*
    * Tematic genre and demographic genre
    */


    for (var j=0; j<radio_type.length; j++) {
        var current_radio = radio_type[j];
        current_radio.onchange = function (event) {
            if (this.value == 'genre')
                aditional_options.style.display = 'block';
            else {
                reset_type_genre(this.value);
            }

        }
    }
    select_style_category.onchange = function (event) {
        var index_selected = this.selectedIndex;
        var opt_selected = this.options[index_selected];
        if (opt_selected.value != "") {
            contener_types_genre.style.display = 'block';
            set_or_remove_attribute_required(radio_types_genre, true);
        } else {
            contener_types_genre.style.display = 'none';
            set_or_remove_attribute_required(radio_types_genre, false);
        }
        uncheck_radio(radio_types_genre);
    }


    // Functions UTILS

    function reset_form () {
        remove_content_of_element(div_edit_item);
        title_from_edit.innerHTML = '';
        contener_actions_edit.style.display = 'none';
        name_en.value = '';
        name_es.value = '';
        description_en.value = '';
        description_es.value = '';
        uncheck_radio(radio_type);
        reset_type_genre(null);
    }

    function reset_type_genre (type) {
        if (type == 'genre')
            aditional_options.style.display = 'block';
        else
            aditional_options.style.display = 'none';
        contener_types_genre.style.display = 'none';
        select_style_category.options[0].selected = true;
        emitEvent(select_style_category, 'change');
        set_or_remove_attribute_required(radio_types_genre, false);
        uncheck_radio(radio_types_genre);
    }

    function set_or_remove_attribute_required (set_radio, add) {
        for (var a=0; a<set_radio.length; a++) {
            var current_type_genre = set_radio[a];
            if (add == true)
                current_type_genre.setAttribute('required', "");
            else {
                current_type_genre.removeAttribute('required');
            }
        }
    }

    function remove_content_of_element (element) {
        while (element.lastChild) {
          element.removeChild(element.lastChild);
        }
    }

    function uncheck_radio (set_radio) {
        for (var a=0; a<set_radio.length; a++) {
            var current_type_genre = set_radio[a];
            current_type_genre.checked = false;
        }
    }

    function emitEvent(element, type) {
        if ("createEvent" in document) {
            var event = document.createEvent("HTMLEvents");
            event.initEvent(type, false, true);
            element.dispatchEvent(event);
        }
        else
            element.fireEvent("on"+type);
    }
}

// Comprobar si "document" está ya cargado (estaría en navegacion ajax)
// o, por lo contrario, añadir un listener para "window"

if (document.readyState == 'complete') {
    genre_loaded();
} else {
    window.addEventListener('load', genre_loaded);
}