/**
 * Created by Emilio on 18/02/14.
 */

actual_input_focus = null;
focus_in_suggest = false;
num_actual_suggest_selected = 0;
count_buttons_focus = 0;
actual_suggest_selected = null;
actual_position = window.getSelection();

function init_search_input_es (where, input_search, div_suggester, query_to_search, field_to_display, custom_remove_textNodes, source_field_es, click_suggest, default_behavior) {
    var div_suggester = div_suggester

    input_search.addEventListener('input', function(event){
        var json_query = event_input_element(event);
        search_in_elasticsearch(json_query, where, field_to_display);
    }, false);
    input_search.addEventListener('focus', function(event){
        event_focus_element(this);
    },false);
    input_search.addEventListener('blur', function(event){
        event_blur_element();
    }, false);
    input_search.addEventListener('keydown', function(event){
        event_keydown_element(event);
    }, false);
    if (default_behavior)  {
        input_search.addEventListener('keyup', function(event){
            event_keyup_element(event);
        }, false);
    }

    // Eventos

    function event_input_element (event) {
        var target = event.target;
        actual_position = window.getSelection();
        if (actual_position.rangeCount > 0) {
            actual_position = actual_position.getRangeAt(0);
            var node = actual_position.commonAncestorContainer;
            if (node.nodeType == Node.TEXT_NODE)
                var target = node
        }
        if (target.value != undefined)
            var text = target.value;
        else
            var text = target.textContent;
        var json_query = query_to_search(text);
        return json_query;
    }

    function event_focus_element (element) {
        num_actual_suggest_selected = 0;
        count_buttons_focus = element.children.length;
        var pos_y = element.offsetTop+element.offsetHeight;
        var pos_x = element.offsetLeft;
        div_suggester.style.position = "absolute";
        div_suggester.style.display = "inline-block";
        div_suggester.style.top = pos_y+"px";
        div_suggester.style.left = pos_x+"px";
        div_suggester.style.width = element.offsetWidth+"px";
        actual_input_focus = element;
    }

    function event_blur_element () {
        if (focus_in_suggest != true) {
            if (typeof custom_remove_textNodes == 'function')
                custom_remove_textNodes();
            else
                remove_textNodes();
            remove_content_of_element(div_suggester);
            div_suggester.style.display = 'none';
        }
    }

    function event_keydown_element (event) {
        var key = event.keyCode || event.charCode;
        if (key == 13) {
            // enter
            event.preventDefault();
            if (div_suggester.children.length > 0) {
                var suggest_selected = document.getElementsByClassName('suggest selected')[0];
                remove_content_of_element(div_suggester);
                div_suggester.style.display = 'none';
                if (typeof custom_remove_textNodes == 'function')
                    custom_remove_textNodes();
                else
                    remove_textNodes();
                if (typeof click_suggest == 'function')
                    click_suggest(suggest_selected);
                else
                    create_and_add_button_to_input(suggest_selected);
            }
        } else if (key == 40 || key == 38) {
            // flecha arriba y abajo
            if (div_suggester.children.length > 0) {
                event.preventDefault();
                navigate_through_suggester(div_suggester, key);
            }
        }
    }

    if (default_behavior) {
        create_and_add_button_to_input = function (suggest_clicked, current_element) {
            // eliminamos solo los nodos "TEXT"
            remove_textNodes();
            // insertamos el boton
            var button = create_input('button', 'button_professional', null, null);
            button.value = suggest_clicked.innerHTML;
            button.removeAttribute('name');
            button.setAttribute('name', suggest_clicked.id);
            if (current_element) {
                // se está llamado a esta
                // funcion para editar un film
                actual_input_focus = current_element;
                current_element.appendChild(button);
                // insertamos el input hidden
                reorganizer_position(current_element);
            } else {
                // se trata entonces de insertar film
                actual_position.insertNode(button);
                // focus en el contenteditable que estabamos
                // y con el cursor al final
                place_cursor_at_end(actual_input_focus);
                // insertamos el input hidden
                reorganizer_position();
                // borramos, ocultamos el suggester y añadimos un espacio
                remove_content_of_element(div_suggester);
            }
            count_buttons_focus = actual_input_focus.children.length;
            return true;
        }

        function event_keyup_element (event) {
            var key = event.keyCode || event.charCode;
            if( key == 8 || key == 46 ) {
                // retroceso
                var compare_count_buttons = actual_input_focus.children.length;
                if (count_buttons_focus != compare_count_buttons) {
                    count_buttons_focus = compare_count_buttons;
                    reorganizer_position();
                }
            }
        }

        function reorganizer_position (current_element) {
            var input_focus = current_element || document.activeElement;
            var buttons = input_focus.getElementsByClassName('button_professional');
            var inputs_hidden = document.getElementById(input_focus.getAttribute('data-id'));
            // reseteamos el contenedor de los inputs hidden correspondiente al input text
            remove_content_of_element(inputs_hidden);
            for (var i=0; i<buttons.length; i++) {
                var button = buttons[i];
                // creamos inputs y los añadimos al contenedor recien reseteado
                var name_input_focus = actual_input_focus.getAttribute('data-id');
                var input_id = create_input('hidden', null, name_input_focus+'['+i+']', null);
                input_id.removeAttribute('class');
                input_id.value = button.getAttribute('name');
                inputs_hidden.appendChild(input_id);
            }
        }
        /*
        function reorganizer_position () {
            var input_focus = document.activeElement;
            var buttons = input_focus.getElementsByClassName('button_professional');
            var inputs_hidden = document.getElementById(input_focus.getAttribute('data-id'));
            // reseteamos el contenedor de los inputs hidden correspondiente al input text
            remove_content_of_element(inputs_hidden);
            for (var i=0; i<buttons.length; i++) {
                var button = buttons[i];
                // creamos inputs y los añadimos al contenedor recien reseteado
                var name_input_focus = actual_input_focus.getAttribute('data-id');
                var input_id = create_input('hidden', null, name_input_focus+'['+i+'].id', null);
                var input_pos = create_input('hidden', null, name_input_focus+'['+i+'].position', null);
                input_id.removeAttribute('class');
                input_pos.removeAttribute('class');
                input_id.value = button.id;
                input_pos.value = i;
                inputs_hidden.appendChild(input_id);
                inputs_hidden.appendChild(input_pos);
            }
        }
        */

        function place_cursor_at_end(element) {
            element.focus();
            if (typeof window.getSelection != "undefined"
                    && typeof document.createRange != "undefined") {
                var range = document.createRange();
                range.selectNodeContents(element);
                range.collapse(false);
                var selection = window.getSelection();
                selection.removeAllRanges();
                selection.addRange(range);
            } else if (typeof document.body.createTextRange != "undefined") {
                var textRange = document.body.createTextRange();
                textRange.moveToElementText(element);
                textRange.collapse(false);
                textRange.select();
            }
        }

        function create_input (type, classname, name, width) {
            var input = document.createElement('input');
            input.setAttribute('type', type);
            input.setAttribute('class', classname);
            input.setAttribute('name', 'add.'+name);
            input.style.width = width;
            return input;
        }

        function remove_textNodes () {
            for (var i=0; i<actual_input_focus.childNodes.length; i++) {
                var element = actual_input_focus.childNodes[i];
                if (element.nodeType == Node.TEXT_NODE) {
                    element.remove();
                    return true;
                }
            }
        }
    }


    function navigate_through_suggester(contener, key) {
        var count_suggests = contener.childNodes.length;
        actual_suggest_selected.setAttribute('class', 'suggest');
        if (key == 40) {
            // flecha abajo
            if (num_actual_suggest_selected < count_suggests-1) {
                num_actual_suggest_selected++;
            }
        } else if (key == 38) {
            // flecha arriba
            if (num_actual_suggest_selected > 0) {
                num_actual_suggest_selected--;
            }
        }
        var current_suggest = contener.childNodes[num_actual_suggest_selected];
        current_suggest.setAttribute('class', 'suggest selected');
        actual_suggest_selected = current_suggest;
    }

    // Search Functions


    function search_in_elasticsearch (input_text, type, field) {
        var data_url = encodeURI("http://"+window.location.hostname+':9200/mongoindex/'+type+'/_search?search_type=dfs_query_and_fetch&source={'+input_text+'}');
        var xhr = new XMLHttpRequest();
        //console.log(data_url)
        xhr.onloadend = function() {
            if (xhr.status == 200) {
                var json_obj = JSON.parse(xhr.responseText);
                if (json_obj.hits.total != 0) {
                    create_suggesters(json_obj, field);
                    console.log("encontrado");
                    return json_obj;
                } else {
                    remove_content_of_element(div_suggester);
                    console.log("no se encuentra");
                    return false;
                }
            }
        }
        xhr.open("GET", data_url, true);
        xhr.send();
    }

    function create_suggesters (json, field) {
        remove_content_of_element(div_suggester);
        div_suggester.style.display = "inline-block";
        json = json["hits"]["hits"];
        num_actual_suggest_selected = 0;
        var code_lang = ['es','es_ES', 'en', 'en_US', 'en_GB'];
        for (var x in json) {
            var actual_suggest = json[x];
            var id = actual_suggest[source_field_es]['_id'];
            var split_field = field.split('.');
            var text_to_display = actual_suggest[source_field_es];
            if (source_field_es == 'fields') {
                if (field == 'name' || field == 'title') {
                    for (var n in code_lang) {
                        var current_lang = code_lang[n];
                        if (text_to_display[field+'.'+current_lang]) {
                            text_to_display = text_to_display[field+'.'+current_lang];
                            break;
                        }
                    }
                } else {
                    text_to_display = text_to_display[field];
                }
            } else if (source_field_es == '_source') {
                if (field == 'title' || field == 'name') {
                    // There that looking for match any languague in 'code_lang' array
                    for (var s=0; s<split_field.length; s++) {
                        var current_field = split_field[s];
                        if (s+1 == split_field.length) {
                            for (var l in code_lang) {
                                var current_lang = code_lang[l];
                                if (text_to_display[current_field][current_lang]) {
                                    text_to_display = text_to_display[current_field][current_lang];
                                    break;
                                }
                            }
                        } else {
                            text_to_display = text_to_display[current_field];
                        }
                    }
                } else {
                    text_to_display = text_to_display[field];
                }
            }
            if (x == 0) {
                var suggest = create_div(id, "suggest selected");
                actual_suggest_selected = suggest;
            } else {
                var suggest = create_div(id, "suggest");
            }
            suggest.setAttribute('role', 'menuitem');
            suggest.setAttribute('tabindex', "-1");
            suggest.data = actual_suggest[source_field_es];
            suggest.num = x;
            suggest.innerHTML = text_to_display;

            suggest.addEventListener('mousedown', function(event){
                focus_in_suggest = true;
            }, false);

            suggest.addEventListener('mouseup', function(event){
                if (typeof custom_remove_textNodes == 'function')
                    custom_remove_textNodes();
                else
                    remove_textNodes();
                div_suggester.style.display = 'none';
                if (typeof click_suggest == 'function')
                    click_suggest(this);
                else
                    create_and_add_button_to_input(this);
            }, false);

            suggest.addEventListener('blur', function(event){
                if (typeof custom_remove_textNodes == 'function')
                    custom_remove_textNodes();
                else
                    remove_textNodes();
                remove_content_of_element(div_suggester);
                div_suggester.style.display = 'none';
            }, false);

            suggest.addEventListener('mouseover', function(event){
                if (actual_suggest_selected != this)
                    actual_suggest_selected.setAttribute('class', 'suggest');
                this.setAttribute('class', 'suggest selected');
                actual_suggest_selected = this;
                num_actual_suggest_selected = parseInt(this.num);
            }, false);

            div_suggester.appendChild(suggest);
        }
    }

    function remove_content_of_element (element) {
        focus_in_suggest = false;
        while (element.lastChild) {
          element.removeChild(element.lastChild);
        }
    }

    function create_div (id, classname) {
        var div = document.createElement('div');
        div.setAttribute('id', id);
        div.setAttribute('class', classname);
        return div;
    }
}