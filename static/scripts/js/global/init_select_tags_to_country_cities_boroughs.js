function init_select_tags_to_country_cities_boroughs (fill_citizenship) {
    check_auto_citizenship = fill_citizenship;

    function get_all_countries () {
        var data_url = encodeURI("http://"+window.location.host+'/api/COUNTRIES/find?search={}');
        var xhr = new XMLHttpRequest();
        xhr.onloadend = function() {
            if (xhr.status == 200) {
                var json = JSON.parse(xhr.responseText);
                if (json) {
                    fill_select_countries(json);
                    return json;
                } else {
                    return false;
                }
            }
        }
        xhr.open("GET", data_url, false);
        xhr.send();
    }

    // ELEMENTS AND GLOBAL VARIABLES

    if (fill_citizenship == true)
        var contener_citizenships = document.getElementById('citizenships');
    var select_country = document.getElementById('countries');
    var select_city = document.getElementById('cities');
    var select_borough = document.getElementById('boroughs');
    var all_countries = null;
    var country_selected = null;
    var id_country_selected = null;

    // EVENTS LISTENERS

    select_country.addEventListener('change', function (event) {
        var index_country_selected = event.target.selectedIndex;
        id_country_selected = event.target.options[index_country_selected].value;
        select_city.style.display = 'none';
        select_borough.style.display = 'none';
        delete_content(select_city);
        delete_content(select_borough);

        if (fill_citizenship == true) {
            uncheckall_citizenships();
            var checkbox_citizenship = document.getElementById('checkbox_'+id_country_selected);
            checkbox_citizenship.checked = check_auto_citizenship;
        }
        for (var x in all_countries) {
            var country = all_countries[x];
            if (country['_id'] == id_country_selected) {
                country_selected = country;
                break;
            }
        }
        if (country_selected != null) {
            if (country_selected['cities'].length > 0)
                select_city.style.display = 'inline';
            for (var y in country_selected['cities']) {
                var current_city = country_selected['cities'][y];
                create_and_append_option(current_city, select_city);
            }
        } else
            console.log("el pais no existe o no ha sido encontrado")
    }, false);

    select_city.addEventListener('change', function (event) {
        var index_city_selected = event.target.selectedIndex;
        var id_city_selected = event.target.options[index_city_selected].value;
        select_borough.style.display = 'none';
        delete_content(select_borough);
        for (var x in country_selected['boroughs']) {
            var current_borough = country_selected['boroughs'][x];
            if (current_borough['_id_city'] == id_city_selected) {
                create_and_append_option(current_borough, select_borough);
            }
            if (select_borough.childElementCount > 1)
                select_borough.style.display = 'inline';
        }
    }, false);


    // [INICIO]

    function fill_select_countries (json) {
        all_countries = json;
        for (var x in all_countries) {
            var current_country = all_countries[x];
            var citizenship_country = current_country['citizenship'];
            create_and_append_option(current_country, select_country);
            if (fill_citizenship == true)
                fill_citizenships(citizenship_country, current_country['_id']);
        }
    }
    get_all_countries();

    function fill_citizenships (citizenship, id_country) {
        var label = document.createElement('label');
        var input_checkbox = document.createElement('input');
        if (citizenship['es'])
            label.textContent = citizenship['es'];
        else
            label.textContent = citizenship['en'];
        label.setAttribute('class', 'box_span_label');
        input_checkbox.value = id_country;
        input_checkbox.name = 'professional.citizenships[]';
        input_checkbox.setAttribute('type', 'checkbox');
        input_checkbox.id = 'checkbox_'+id_country;
        label.appendChild(input_checkbox);
        contener_citizenships.appendChild(label);
        contener_citizenships.innerHTML += " ";
    }

    // FUNCTIONS UTILS

    function uncheckall_citizenships () {
        var checkboxs_citizenship = contener_citizenships.getElementsByTagName('input');
        for (var i=0; i<checkboxs_citizenship.length;i++) {
            var current_checkbox = checkboxs_citizenship[i];
            current_checkbox.checked = false;
        }
    }

    function create_and_append_option (current_element, select) {
        var create_option = document.createElement('option');
        create_option.value = current_element['_id'];
        if (current_element['name']['es'])
            create_option.innerHTML = current_element['name']['es'];
        else
            create_option.innerHTML = current_element['name']['en'];
        select.appendChild(create_option);
    }

    function delete_content (contener) {
        // borar todos salvo el primer child 'option' (el default)
        while (contener.lastChild && contener.children.length > 1) {
          contener.removeChild(contener.lastChild);
        }
    }
}