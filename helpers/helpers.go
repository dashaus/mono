package helpers

import (
	"github.com/gorilla/mux"
)

func GetURLRoute (router *mux.Router, nameRoute string, pairs ...string) string {
	route, err := router.Get(nameRoute).URL(pairs...)
	if err != nil {
		panic(err)
	}
	name := route.Path
	return name
}
