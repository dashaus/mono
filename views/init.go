package views

import (
	"github.com/gorilla/mux"
	"html/template"
	"net/http"
	"encoding/json"
	"fmt"
	"bytes"
)

var Router = mux.NewRouter()
//var store = sessions.NewCookieStore([]byte("oraculum"))

type JSONReturn struct {
	Html	string
	Section	string
}


// INIT
var pathTemplates = "templates/"
var templates = make(map[string]*template.Template)
func init () { // init() implicitly execute first
	templates["home"] = template.Must(template.ParseFiles(pathTemplates+"generals/home/news/article.html", pathTemplates+"generals/home/base_home.html", pathTemplates+"base.html"))
	templates["home/article"] = template.Must(template.ParseFiles(pathTemplates+"generals/home/news/article.html", pathTemplates+"generals/home/news/article_details.html", pathTemplates+"base.html"))
	templates["vitra/index"] = template.Must(template.ParseFiles(pathTemplates+"vitra/index.html", pathTemplates+"base.html"))
	templates["vitra/cinema/film/add_edit"] = template.Must(template.ParseFiles(pathTemplates+"vitra/cinema/film/add_edit.html", pathTemplates+"base.html"))
	templates["errors/notfound"] = template.Must(template.ParseFiles(pathTemplates+"_errors/notfound.html", pathTemplates+"base.html"))
}

// FUNCTIONS
func RenderTemplate (resWriter http.ResponseWriter, request *http.Request, templateName *template.Template, sectionName string, vars interface{}) {
	isAjax := request.Header.Get("X-Requested-With")
	//var name string // hace referencia a la parte a cargar ya sea "define" o "template" en el template asociado

	if isAjax == "XMLHttpRequest" {
		fmt.Println("la petición es AJAX")
		resWriter.Header().Set("Content-Type", "application/json")
		var outputHTML bytes.Buffer
		err := templateName.ExecuteTemplate(&outputHTML, "content", vars)
		if err != nil {
			http.Error(resWriter, err.Error(), http.StatusInternalServerError)
			return
		}
		jsonToEncode := JSONReturn{Html: outputHTML.String(), Section: sectionName}
		jsonEncoded, err := json.Marshal(jsonToEncode)
		if err != nil {
			http.Error(resWriter, err.Error(), http.StatusInternalServerError)
			return
		}
		resWriter.Write(jsonEncoded)
	} else {
		resWriter.Header().Set("Content-Type", "text/html")
		err := templateName.ExecuteTemplate(resWriter, "base", vars) // "base" make reference at name of "define" inside of "base.html"
		if err != nil {
			http.Error(resWriter, err.Error(), http.StatusInternalServerError)
			return
		}
	}
}
