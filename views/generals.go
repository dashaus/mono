package views

import (
	"net/http"
	"bitbucket.org/dashaus/mono/models"
	"github.com/gorilla/mux"
	"strconv"
	"labix.org/v2/mgo/bson"
	"bitbucket.org/dashaus/mono/helpers"
)

// VIEWS
// --------------------------------------------------------------------------------------
func HomeView (resWriter http.ResponseWriter, request *http.Request) {
	query := bson.M{"forum": "noticias"}
	makeData := func () map[string]interface{} {
		data := make(map[string]interface{})
		resultQuery := models.GetAllNews("posts", &query, true)
		// include the URL of each news generated from its own route: "newsArticle"
		for i := range resultQuery {
			article := &resultQuery[i]
			datetime, slug := article.Last_message_published, article.Slug
			year, month, day := strconv.Itoa(datetime.Year()),
			strconv.Itoa(int(datetime.Month())), strconv.Itoa(datetime.Day())
			link := helpers.GetURLRoute(Router, "newsArticle", "year", year , // handle "error"
				"month", month, "day", day, "slug", slug)
			article.URL = link
		}
		data["db"] = resultQuery
		return data
	}
	RenderTemplate(resWriter, request, templates["home"], "home", makeData())
}

func NewsArticleView (resWriter http.ResponseWriter, request *http.Request) {
	matchmap := mux.Vars(request)
	query := bson.M{"forum": "noticias", "slug":matchmap["slug"]}
	makeData := func () map[string]interface{} {
		data := make(map[string]interface{})
		article := models.GetOneNews("posts", &query)
		data["db"] = article
		return data
	}
	RenderTemplate(resWriter, request, templates["home/article"], "vitraHome", makeData())
}
