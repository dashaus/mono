package views

import (
	"net/http"
)

// VIEWS
// --------------------------------------------------------------------------------------
func NotFoundView (resWriter http.ResponseWriter, request *http.Request) {
	RenderTemplate(resWriter, request, templates["errors/notfound"], "notFound", nil)
}
