package views

import (
	"net/http"
	"encoding/json"
	"bitbucket.org/dashaus/mono/models"
	"github.com/gorilla/mux"
	"net/url"
)

// VIEWS
// --------------------------------------------------------------------------------------
func APIView (resWriter http.ResponseWriter, request *http.Request) {
	resWriter.Header().Set("Content-Type", "application/json")
	varsURL := mux.Vars(request)
	var writeInHtml []byte
	if varsURL["command"] == "find" || varsURL["command"] == "find_one" {
		error := false
		query := make(map[string]interface {})
		qs, err := url.ParseQuery(request.URL.RawQuery)
		if err != nil {
			panic(err)
		}
		if len(qs) > 0 {
			if key, ok := qs["search"]; ok {
				query = make(map[string]interface {})
				err := json.Unmarshal([]byte(key[0]), &query)
				if err != nil {
					error = true
					writeInHtml = []byte("Invalid query.")
				}
			} else {
				error = true
				writeInHtml = []byte("The name action should be 'search'.")
			}
		}
		if !error {
			result := models.GetAll(varsURL["collection"], &query)
			writeInHtml, err = json.Marshal(&result)
			if err != nil {
				http.Error(resWriter, err.Error(), http.StatusInternalServerError)
				return
			}
		}
	} else {
		writeInHtml = []byte("database comand not valid")
	}
	resWriter.Write(writeInHtml)
}


