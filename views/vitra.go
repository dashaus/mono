package views

import (
	"net/http"
	"bitbucket.org/dashaus/mono/helpers"
	"bitbucket.org/dashaus/mono/models"
	"labix.org/v2/mgo/bson"
	"fmt"
)

type vitraMainURLs struct {
	News string
	Film string
	EpisodieInFilm string
	Saga string
	FormatAndOthers string
	GenresTopics string
	CutVersion string
	Classification string
	Professional string
	Occupation string
	Book string
	Gala string
	TypeGala string
	CountriesAndCities string
}

// VIEWS
// --------------------------------------------------------------------------------------
func VitraIndexView (resWriter http.ResponseWriter, request *http.Request) {
	URLs := &vitraMainURLs{
		News: helpers.GetURLRoute(Router, "vNews"),
		Film: helpers.GetURLRoute(Router, "vCinemaFilmAddEdit"),
		Saga: helpers.GetURLRoute(Router, "vCinemaFilmSagaAddEdit"),
		EpisodieInFilm: helpers.GetURLRoute(Router, "vCinemaFilmEpisodeSearch"),
		FormatAndOthers: helpers.GetURLRoute(Router, "vCinemaSpecsAdd"),
		GenresTopics: helpers.GetURLRoute(Router, "vCinemaGenreAddEdit"),
		CutVersion: helpers.GetURLRoute(Router, "vCinemaCutAddEdit"),
		Classification: helpers.GetURLRoute(Router, "vCinemaClassificationAddEdit"),
		Professional: helpers.GetURLRoute(Router, "vCinemaProfessionalAddEdit"),
		Occupation: helpers.GetURLRoute(Router, "vCinemaProfessionalOccupationAddEdit"),
		Book: helpers.GetURLRoute(Router, "vCinemaBookAddEdit"),
		Gala: helpers.GetURLRoute(Router, "vCinemaGalaAdd"),
		TypeGala: helpers.GetURLRoute(Router, "vCinemaTyeGalaAddEdit"),
		CountriesAndCities: helpers.GetURLRoute(Router, "vGeneralsCountryAdd"),
	}
	RenderTemplate(resWriter, request, templates["vitra/index"], "vitraHome", URLs)
}

func VitraFilmAddEditView (resWriter http.ResponseWriter, request *http.Request) {
	errors := make(map[string]string)
	data := make(map[string]interface{})
	data["errors"] = errors
	makeData := func () map[string]interface{} {
		data["urlForm"] = helpers.GetURLRoute(Router, "vCinemaFilmAddEdit")
		data["db_versioncut"] = models.GetAll(models.CVersionCutName, &bson.M{})
		data["db_classification"] = models.GetAll(models.CClassificationName, &bson.M{})
		dbGenresTopics := models.GetAll(models.CGenresTopicsName, &bson.M{})
		dbFormatAndOthers := models.GetAll(models.CFormatAndOthersName, &bson.M{})
		// SEPARATE EACH TYPE IN A MAP WITH SLICE INSIDE
		separateType := func (dbResults []interface {}, mapTo map[string]interface {}) {
			for k := range dbResults {
				current := dbResults[k].(bson.M)
				name := current["type"].(string)
				mapTo[name] = append(mapTo[name].([]interface {}), current)
			}
		}
		sFormatAndOthers := map[string]interface{}{ // ("format, category, production")
			"format": []interface {}{},
			"category": []interface {}{},
			"production": []interface {}{},
		}

		sGenresTopics := map[string]interface{}{ // ("genres", "topics")
			"genre": []interface {}{},
			"topic": []interface {}{},
		}
		separateType(dbFormatAndOthers, sFormatAndOthers)
		separateType(dbGenresTopics, sGenresTopics)
		data["db_formatandothers"] = sFormatAndOthers
		data["db_genrestopics"] = sGenresTopics
		return data
	}
	if request.Method == "POST" {
		fmt.Println("es un POST!!!")
	}
	RenderTemplate(resWriter, request, templates["vitra/cinema/film/add_edit"],
		"vitraCinemaFilmAddEdit", makeData())
}
