package models

import (
	"labix.org/v2/mgo"
	"labix.org/v2/mgo/bson"
	"time"
	"html/template"
)

const (
	databaseName = "monoculum"
	CVersionCutName = "FILM_VERSION_CUT"
	CFormatAndOthersName = "FILM_FORMAT_CATEGORY_PRODUCTION"
	CGenresTopicsName = "FILM_GENRES_TOPICS"
	CClassificationName = "FILM_CLASSIFICATION"
)

type FullArticle struct {
	Title string
	Author string
	Preview string
	Body template.HTML
	Image string
	Length_comment int
	Last_message_published time.Time
	Slug string
}
type PreviewArticle struct {
	Title string
	Author string
	Preview string
	Image string
	Length_comment int
	Last_message_published time.Time
	Slug string
	URL string
}

type OneCutVersion struct {
	Name string
}


// METHODS
// --------------------------------------------------------------------------------------
func (a *FullArticle) CheckFields () map[string]bool  {
	options := make(map[string]bool, 2)
	options["body"] = true
	options["url"] = false
	return options
}
func (s *PreviewArticle) CheckFields () map[string]bool  {
	options := make(map[string]bool, 2)
	options["body"] = false
	options["url"] = true
	return options
}


// CONNECTION BASE
// --------------------------------------------------------------------------------------
func getSession () *mgo.Session {
	session, err := mgo.Dial("127.4.116.130:27017")
	if err != nil {
		panic(err)
	}
	return session
}

func withCollection(collection string, s func(*mgo.Collection) error) error {
	session := getSession()
	defer session.Close()
	c := session.DB(databaseName).C(collection)
	return s(c)
}


// FUNCTION QUERIES
// --------------------------------------------------------------------------------------
// Generals
func GetAll (collectionName string, query interface {}) ([]interface {}) {
	var result []interface {}
	queryFunc := func(c *mgo.Collection) error {
		fn := c.Find(query).All(&result)
		return fn
	}
	err := withCollection(collectionName, queryFunc)
	if err != nil {
		panic(err) // "Database Error"
	}
	return result
}

// Specificis
// - HomeView,
func GetAllNews (collectionName string,
	query *bson.M, sort bool) (result []PreviewArticle)  {
	result = []PreviewArticle{}
	queryFunc := func(c *mgo.Collection) error {
		var fn error
		if sort {
			fn = c.Find(query).Sort("-_id").All(&result)
		} else {
			fn = c.Find(query).All(&result)
		}
		return fn
	}
	err := withCollection(collectionName, queryFunc)
	if err != nil {
		panic(err) // "Database Error"
	}
	return
}
// - NewsArticleView,
func GetOneNews (collectionName string, query *bson.M) (result *FullArticle)  {
	result = &FullArticle{}
	queryFunc := func(c *mgo.Collection) error {
		fn := c.Find(query).One(&result)
		return fn
	}
	err := withCollection(collectionName, queryFunc)
	if err != nil {
		panic(err) // "Database Error"
	}
	return result
}

// - VitraFilmAddEditView,
func GetOneCutVersion (collectionName string, query *bson.M) (result *OneCutVersion)  {
	result = &OneCutVersion{}
	queryFunc := func(c *mgo.Collection) error {
		fn := c.Find(query).One(&result)
		return fn
	}
	err := withCollection(collectionName, queryFunc)
	if err != nil {
		panic(err) // "Database Error"
	}
	return result
}
